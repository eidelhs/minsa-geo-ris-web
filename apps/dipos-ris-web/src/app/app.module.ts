import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MarketGuardsModule, MarketServicesModule } from '@dipos-ris-web-commons';
import { NotFoundComponent } from './core/components/not-found/not-found.component';
import { HeaderComponent } from './core/components/header/header.component';
import { SidebarComponent } from './core/components/sidebar/sidebar.component';
import { AdminSharedModule } from './features/admin/shared/shared.module';
import { WebsiteComponent } from './features/website/website.component';
import { AdminComponent } from './features/admin/admin.component';

@NgModule({
  declarations: [
    AppComponent,
   // HeaderComponent,
    SidebarComponent,
    NotFoundComponent,
    WebsiteComponent,
    AdminComponent
  ],
  imports: [
    BrowserModule,
    MarketGuardsModule,
    MarketServicesModule,
    AppRoutingModule,
    AdminSharedModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      closeButton: true,
      timeOut: 3000,
      progressBar: true,
      // positionClass: 'toast-bottom-right',
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
 // schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
