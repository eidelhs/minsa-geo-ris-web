import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SessionService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;

  @Input() toggle: boolean;

  @Output() toggleChange = new EventEmitter<boolean>();

  keywords = new FormControl();

  searchOpen: boolean;

  user: any;

  constructor(
    private session: SessionService,
    private router: Router
  ) { }

  ngOnInit() {
    this.user = this.session.user;
  }

  toggleActive(): void {
    this.toggle = !this.toggle;
    localStorage.setItem('sidebar', this.toggle ? 'open' : 'close');
    this.toggleChange.emit(this.toggle);

    // Trigger for resize apx-graphs
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 250);
  }

  clear() {
    this.keywords.reset();
    // this.inputSearch.nativeElement.focus();
    this.searchOpen = true;
  }

  signOut() {
    this.session.destroy();
    this.user = [];
    this.router.navigateByUrl('/website');
  }

}
