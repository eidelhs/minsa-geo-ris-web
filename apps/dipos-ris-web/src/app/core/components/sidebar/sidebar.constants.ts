
import { ISidebarMenu } from '../../../features/admin/interfaces/menu.interface';

export const options: ISidebarMenu[] = [
  {
    title: 'Inicio',
    role: 'publico',
    childrens:[
     { title: 'Inicio',
     url: '/website/inicio'}
    ]
  },
  /** ADD MENÚ ETAPAS   */

  //#region Etapa 0

  {
    title: 'Etapa 0: Definición de Política <br> y Marco Normativo',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      {
        title: 'Fase 1: Política Sectorial RIS',
        role: 'publico',
        childrens: [
          {
            title: 'Política Nacional Multisectorial de Salud (DS)',
            url: '/etapas/etapas-0/fases/fase-1/Politica-Nacional-Multisectorial-Salud',
          },
          {
            title: 'Política General de Gobierno',
            url: '/etapas/etapas-0/fases/fase-1/Politica-General-Gobierno',
          },
          {
            title: 'Ley 30885',
            url: '/etapas/etapas-0/fases/fase-1/Ley30885',
          },
        ],
      },
      {
        title: 'Fase 2:  Emisión del marco normativo de la Política Sectorial',
        role: 'publico',
        //url: '/Implementacion',
        childrens: [
          {
            title:
              'Documentos normativos específicos para laregulación del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Especificos-Regularizacion-Modelo-RIS',
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “prestación” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
             
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gestión” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Gestion-Modelo-RIS',
             
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “financiamiento” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Financiamiento-Modelo-RIS',
             
          },
          {
            title:
              'Documentos normativos relacionados a la dimensión “gobernanza” del Modelo RIS',
            url: '/etapas/etapas-0/fases/fase-2/Documentos-Normativos-Relacionados-Dimension-Gobernanza-Modelo-RIS',
             
          },
          {
            title: 'Abogacía del Comité Impulsor Nacional de las RIS',
            url: '/etapas/etapas-0/fases/fase-2/Abogacia-Comite-Impulsor-Nacional-RIS',
             
          },
          {
            title: 'RM del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/RM-COINRIS',
             
          },
          {
            title: 'Plan de Trabajo del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Plan-Trabajo-COINRIS',
             
          },
          {
            title: 'Actas del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Actas-COINRIS',
             
          },
          {
            title: 'Informes del COINRIS',
            url: '/etapas/etapas-0/fases/fase-2/Informes-COINRIS',
             
          },
        ],
      },
      {
        title: 'Fase 3: Abogacía para la RIS',
        role: 'publico',
        //url: '/Implementacion',
         
        childrens: [
          {
            title: 'Otra Opción 1',
            url: '/Etapa-0/Fase-3/Otra-Opcion-1',
             
          },
          {
            title: 'Otra Opción 2',
            url: '/Etapa-0/Fase-3/Otra-Opcion-2',
             
          },
        ],
      },
      {
        title: 'Fase 4: Adecuación del Marco Normativo',
        role: 'publico',
        //url: '/Implementacion',
         
        childrens: [
          {
            title: 'Otra Opción 1',
            url: '/Etapa-0/Fase-4/Otra-Opcion-1',
             
          },
          {
            title: 'Otra Opción 2',
            url: '/Etapa-0/Fase-4/Otra-Opcion-2',
             
          },
        ],
      },
    ],
  },

  //#endregion Etapa 0

  //#region Etapa 1
  {
    title: 'Etapa 1:  Conformación de la <br> RIS del MINSA y GORE',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      // {
      //   title: 'Cronograma',
      //   url: '/Etapa-0',
      //   icon: 'icon-drop'
      // },
      {
        title: 'Fase 1: Estructuración de las RIS',
        role: 'publico',
        childrens: [
          {
            title:
              'Avances (Mapa del Perú con los avances de la estructuración y resultados por RIS de Línea de Base)',
            url: '/Etapa-1/Fase-1/Avances-Estructuracion-Resultados-RIS-Linea-Base',
             
          },
          {
            title:
              'Avances (Mapa del Perú con las RD de conformación de los EIRIS)',
            url: '/website/equipos-impulsores-de-ris',
             
          },
          {
            title:
              'Mapa de Peru con Ambitos Priorizados por al Autoridad Sanitaria para Estructurar RIS',
            url: '/Etapa-1/Fase-1/Mapa-Peru-Ambitos-Priorizados-Autoridad-Sanitaria-Estructurar-RIS',
             
          },
          {
            title: 'Herramientas',
             
            childrens: [
              {
                title: 'Herramienta de Cartografía de estructuración de RIS',
                url: '/website',
                 
              },
              {
                title: 'Calculador de Tamaño Poblacional',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title:
                  'Matriz de Operacionalización de Criterios Técnicos para definir Unidades Territoriales Sanitarias Población e IPRESS Relacionadas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title:
                  'Instrumento de Medición de Grado de Integración de la RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title:
                  'FICHA DE VISITA A ESTABLECIMIENTO DE SALUD DEL SEGUNDO NIVEL DE ATENCIÓN DE SALUD PARA CORROBORAR INFORMACIÓN SOBRE MEDICIÓN DEL GRADO DE INTEGRACIÓN DE LA RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title:
                  'FICHA DE VISITA A ESTABLECIMIENTO DE SALUD DEL PRIMER NIVEL DE ATENCIÓN DE SALUD PARA CORROBORAR INFORMACIÓN SOBRE MEDICIÓN DEL GRADO DE INTEGRACIÓN DE LA RIS',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
        ],
      },
      {
        title: 'Fase 2:  Formalización de la RIS',
        role: 'publico',
        //url: '/Implementacion',
         
        childrens: [
          {
            title:
              'Avances (Mapa del Perú con las RD de conformación de las RIS)',
            url: '/website/resolucion-de-confirmacion-de-ris',
             
          },
          {
            title: 'Herramientas',
            url: '/Etapa-0/Fase-2/Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
             
          },
          {
            title: 'Modelo del RD',
            url: '/Etapa-0/Fase-2/Componente4',
             
          },
          {
            title: 'Modelo de Informe de Sustentación',
            url: '/Etapa-0/Fase-2/Componente5',
             
          },
        ],
      },      
      {
        title: 'Dashboard',
        role: 'publico',
         
        childrens: [
          {
            title: 'Dashboard',
            url: '/website/dashboard',
             
          }
        ]
      }
    ],
  },
  //#endregion

  //#region Etapa 2
  {
    title: 'Etapa 2:  Desarrollo de las RIS del MINSA y GORE',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      // {
      //   title: 'Cronograma',
      //   url: '/Etapa-0',
      //   icon: 'icon-drop'
      // },
      {
        title: 'Fase 1: Desarrollo Inicial de las RIS',
        role: 'publico',
        childrens: [
          {
            title:
              'subfase 1 Elaboración del Plan de Fortalecimiento de la Capacidad Resolutiva de las RIS',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 2: Adecuación de documentos de gestión',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 3: Conformación y funcionamiento del EGRIS',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 4: Estandarización de procesos',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title:
              'subfase 5: Formulación, ejecución y control del Plan de Salud de las RIS',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 6: Implementación progresiva del SIHCE',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 7: Implementación de telesalud',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'Subfase 8: Fortalecimiento de la instancia de gobernanza',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 9: Incorporación de mecanismos de financiamiento',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 10: Optimización de recursos',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
        ],
      },
      {
        title: 'Fase 2: Desarrollo Gradual de las RIS',
        role: 'publico',
        //url: '/Implementacion',
         
        childrens: [
          {
            title: 'subfase 1: Cierre Gradual de Brecha de EMS',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
          {
            title: 'subfase 2: Cierre Gradual de Cartera de Servicios de Salud',
             
            childrens: [
              {
                title: 'Avances',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
              {
                title: 'Herramientas',
                url: '/Etapa-0/Fase-1/Politica-Nacional-Multisectorial-Salud',
                 
              },
            ],
          },
        ],
      },
    ],
  },
  //#endregion

  //#region Etapa 3
  {
    title: 'Etapa 3: Integración de otras IPRESS públicas, privadas o mixtas',
    icon: 'icon-layer',
    role: 'publico',
    childrens: [
      {
        title: 'Fase 1: Proceso de Homologación para la integración',
        url: '/Etapa-0',
      },
      {
        title: 'Fase 2: Suscripción de convenios de integración a las RIS',
        url: '/Implementacion'
      },
    ],
  },
  //#endregion

  /** FIN MENÚ ETAPAS */
  {
    title: 'Nominalización',
    icon: 'bx-folder',
    role: '"[ROLE_MANAGER]"',
    childrens: [{ title: 'Búsqueda', url: '/admin/nominalizacion' }],
  },
  {
    title: 'Equipos Mult.',
    icon: 'bx-user-plus',
    role: '"[ROLE_MANAGER]"',
    childrens: [
      { title: 'Asignación', url: '/admin/equipos-multidiciplinario-de-salud' },
    ],
  },
  {
    title: 'Usuarios',
    icon: 'bx-user-plus',
    role: '"[ROLE_MANAGER]"',
    childrens: [
      { title: 'Listado', url: '/usuarios' },
    ],
  },
  {
    title: 'PNIRIS',
    icon: 'bx-user-plus',
    role: '"[ROLE_MANAGER]"',
    childrens: [
      { title: 'Matriz de segumiento', url: '/PNIRIS' },
      { title: 'Registrar Matriz', url: '/PNIRIS/app-registrar-metas-fisicas' },
      { title: 'Ver Matriz', url: '/PNIRIS/app-visor-metas-fisicas' },
      { title: 'Supervisar Matriz', url: '/PNIRIS/app-supervisar-metas-fisicas' },
    ],
  },
];
