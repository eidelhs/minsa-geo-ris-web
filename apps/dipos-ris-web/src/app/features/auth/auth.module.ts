import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthComponent } from './auth.component';
import { SignInComponent } from './views/sign-in/sign-in.component';
import { AuthRoutingModule } from './auth-routing.module';
import { MarketHttpModule, MarketServicesModule } from '@dipos-ris-web-commons';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MarketServicesModule,
    MarketHttpModule
  ],
  declarations: [
    AuthComponent,
    SignInComponent
  ]
})
export class AuthModule { }
