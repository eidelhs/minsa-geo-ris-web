import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  show: boolean = true;
  offline: boolean;

  constructor() { }

  ngOnInit() {
    const sidebar = localStorage.getItem('sidebar');
    this.show = !sidebar || sidebar === 'open';

    this.verifySignal();
  }

  verifySignal(): void {
    fromEvent(window, 'online')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = false);

    fromEvent(window, 'offline')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = true);
  }

}
