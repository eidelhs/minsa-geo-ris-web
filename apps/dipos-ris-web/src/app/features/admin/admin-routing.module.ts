import { NgModule } from "@angular/core";
import { Router, RouterModule, Routes } from "@angular/router";
import { RolGuard } from "@dipos-ris-web-commons";
import { UserRol } from "@dipos-ris-web/models";
import { AdminComponent } from "./admin.component";
import { AuthChildsGuard } from "./guards/auth-childs.guard";
import { DashboardComponent } from "./views/dashboard/dashboard.component";
import { UbicaRisComponent } from './views/ubica-ris/ubica-ris.component';
import { UbicaNoRisComponent } from './views/ubica-no-ris/ubica-no-ris.component';
import { NominalizacionComponent } from './views/nominalizacion/nominalizacion.component';
import { TumbesRisComponent } from "./views/tumbes-ris/tumbes-ris.component";
import { TacnaRisComponent } from "./views/tacna-ris/tacna-ris.component";
import { MadreDeDiosRisComponent } from "./views/madre-de-dios-ris/madre-de-dios-ris.component";
import { UcayaliRisComponent } from "./views/ucayali-ris/ucayali-ris.component";
import { EquipoMultidiciplinarioComponent } from "./views/equipo-multidiciplinario/equipo-multidiciplinario.component";

const routes: Routes = [
	{
		path: '',
		component: AdminComponent,
		//canActivateChild: [AuthChildsGuard],
		children: [
			{ path: '', pathMatch: 'full', component: DashboardComponent },
			{
				path: 'dashboard',
				component: DashboardComponent
			},
			{
				path: 'lima-metropolitana-ris',
				component: UbicaRisComponent
			},
			{
				path: 'todo-peru-ris',
				component: UbicaNoRisComponent
			},
			{
				path: 'tumbes-ris',
				component: TumbesRisComponent
			},
			{
				path: 'tacna-ris',
				component: TacnaRisComponent
			},
			{
				path: 'madre-de-dios-ris',
				component: MadreDeDiosRisComponent
			},
			{
				path: 'ucayali-ris',
				component: UcayaliRisComponent
			},
			{
				path: 'nominalizacion',
				component: NominalizacionComponent
			},
			{
				path: 'equipos-multidiciplinario-de-salud',
				component: EquipoMultidiciplinarioComponent
			},
			{
				path: 'ventas',
				loadChildren: () => import('./views/sales/sales.module').then(m => m.SalesModule)
			},
			{
				path: 'configuracion',

				loadChildren: () => import('./views/configuration/configuration.module').then(m => m.ConfigurationModule),
				canActivate: [RolGuard],
				data: { rols: [UserRol.admin] }
			},
			{
				path: 'inventario',
				loadChildren: () => import('./views/inventory/inventory.module').then(m => m.InventoryModule)
			},
			{
				path: 'contactos',
				loadChildren: () => import('./views/customers/customers.module').then(m => m.CustomersModule)
			},
			{
				path: 'caja',
				loadChildren: () => import('./views/moneybox/moneybox.module').then(m => m.MoneyboxModule)
			},			
			{
				path: 'estadisticas',
				loadChildren: () => import('./views/statistics/statistics.module').then(m => m.StatisticsModule),
				canActivate: [RolGuard],
				data: { rols: [UserRol.admin] }
			},
			{
				path: 'reportes',
				loadChildren: () => import('./views/reports/reports.module').then(m => m.ReportsModule),
				canActivate: [RolGuard],
				data: { rols: [UserRol.admin] }
			},
		]
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AdminRoutingModule { }
