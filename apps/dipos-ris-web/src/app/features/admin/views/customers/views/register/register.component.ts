import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService, OwnerService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-customer-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class CustomerRegisterComponent implements OnInit {

  ownerId: number;
  form: FormGroup;
  sales = [];
  salesController: any = {};
  isLoading: boolean;
  isLoadingSales: boolean;

  constructor(
    private fb: FormBuilder,
    private ownerService: OwnerService,
    private orderService: OrderService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.build();
  }

  ngOnInit(): void {
    this.ownerId = +this.activatedRoute.snapshot.paramMap.get('id');
    if (this.ownerId) {
      this.isLoading = true;
      this.ownerService.byId(this.ownerId).subscribe(res => {
        this.form.patchValue(res);
        this.isLoading = false;
      }, () => this.isLoading = false);
    }
  }

  build(): void {
    this.form = this.fb.group({
      IdPropietario: null,
      IdTipoDocumento: null,
      NroDocumento: null,
      RazonSocial: [null, Validators.required],
      Direccion: null,
      Email: [null, Validators.email],
      Celular: null,
      Observaciones: null,
      FechaRegistro: null,
      IdPersonalRegistro: null,
      FlagCliente: true,
      FlagProveedor: false,
      FlagActivo: true
    });
  }

  save(): void {
    if (this.form.valid) {
      const values = this.form.getRawValue();
      values.RazonSocial = values.RazonSocial.toUpperCase();
      values.Email = values.Email?.toLowerCase();

      this.isLoading = true;

      this.ownerService.register(values).subscribe(res => {
        this.form.get('IdPropietario').setValue(res);
        this.isLoading = false;
        this.toastr.success('Los datos se han registrado exitosamente');
      }, () => this.isLoading = false);
    }
  }

  reset(): void {
    this.form.reset({ FlagCliente: true, FlagActivo: true });
    this.sales = [];
    this.router.navigateByUrl('/contactos/registro');
  }

  getData(): void {
    const document = this.form.get('NroDocumento').value;
    this.isLoading = true;
    this.ownerService.getDataByDocument(document).subscribe(res => {
      this.isLoading = false;
      if (res) {
        this.form.patchValue({
          RazonSocial: res.nombre,
          Direccion: res.domicilio,
        });
      }
    }, () => this.isLoading = false);
  }

  getSales(): void {
    if (this.ownerId) {
      this.isLoadingSales = true;
      this.orderService.search({ id_client: this.ownerId, Top: 100 }).subscribe(res => {
        this.isLoadingSales = false;
        this.sales = res;
      }, () => this.isLoadingSales = false);
    }
  }

}
