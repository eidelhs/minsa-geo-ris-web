import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

  @Input() user: any;
  form: FormGroup;
  roles: any[] = [];
  showPass: boolean;

  constructor(
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private userService: UserService
  ) {
    this.build();
  }

  ngOnInit() {
    this.userService.roles().subscribe(res => {
      this.roles = res;
    });

    this.form.patchValue({ ...this.user, Clave: null });
    if (this.user?.IdUsuario) {
      this.form.get('IdUsuario').disable();
    }
  }

  build(): void {
    this.form = this.fb.group({
      IdUsuario: [null, Validators.required],
      IdPersonal: [null, Validators.required],
      IdTipo: [1, Validators.required],
      Clave: [null, Validators.required],
      FlagActivo: true
    });
  }

  save(): void {
    const values = this.form.getRawValue();

    values.IdUsuario = values.IdUsuario.toUpperCase();
    const tipo = this.roles.find(x => x.IdTipo == values.IdTipo);
    values.Tipo = tipo?.Detalle;

    const isCreate = !this.form.get('IdUsuario').disabled;
    this.userService.create(values, isCreate).subscribe(res => {
      if (res) {
        this.bsModal.close(values);
      }
    });
  }

}
