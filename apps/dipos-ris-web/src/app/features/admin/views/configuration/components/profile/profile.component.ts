import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ConfirmService } from '@admin-shared/services/confirm.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PersonalService, UserService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import { UserEditComponent } from '../user-edit/user-edit.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Input() user: any;
  form: FormGroup;
  companies: any[];
  isSave: boolean;

  constructor(
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private confirmService: ConfirmService,
    private personalService: PersonalService,
    private userService: UserService,
    private toastr: ToastrService
  ) {
    this.build();
  }

  ngOnInit() {
    this.personalService.companies().subscribe(res => {
      this.companies = res;
    });

    if (this.user) {
      this.personalService.byID(this.user.IdPersonal).subscribe((res: any) => {
        if (res) {
          if (res.FechaNacimiento) {
            res.FechaNacimiento = new Date(res.FechaNacimiento);
          }

          this.form.patchValue(res);
        }
      });
    }

  }

  build(): void {
    this.form = this.fb.group({
      IdPersonal: null,
      IdEmpresa: [null, Validators.required],
      Nombres: [null, Validators.required],
      ApePaterno: [null, Validators.required],
      ApeMaterno: [null, Validators.required],
      IdTipoDocumento: null,
      NroDocumento: null,
      Genero: null,
      FechaNacimiento: null,
      Celular: null,
      Email: [null, Validators.email],
      Direccion: null,
      FlagActivo: true,
      FechaRegistro: new Date(),
      IdPersonalRegistro: null,
      Usuarios: this.fb.control([])
    });
  }

  title(str: string) {
    return str.toLowerCase().replace(/(^|\s)\S/g, (t) => t.toUpperCase());
  }

  save(): void {
    if (this.form.valid) {
      this.isSave = true;

      const values = this.form.value;
      values.Nombres = this.title(values.Nombres);
      values.ApePaterno = this.title(values.ApePaterno);
      values.ApeMaterno = this.title(values.ApeMaterno);
      values.Email = values.Email?.toLowerCase();

      this.personalService.register(values).subscribe(id => {
        this.form.get('IdPersonal').setValue(id);

        this.toastr.success('Los datos han sido registrados exitosamente');
      });
    }
  }

  accept(): void {
    const values = this.form.value;
    this.bsModal.close(this.isSave ? values : null);
  }

  clear(): void {
    this.form.reset({
      FlagActivo: true,
      FechaRegistro: new Date(),
    });
  }

  addUser(): void {
    const user = { IdPersonal: this.form.get('IdPersonal').value };
    this.bsModal.open(UserEditComponent, { user }).then(res => {
      if (res) {
        this.form.get('Usuarios').value.push(res);
        this.toastr.success('Los datos han sido registrados exitosamente');
      }
    });
  }

  editUser(user: any, index: number) {
    this.bsModal.open(UserEditComponent, { user }).then(res => {
      if (res) {
        this.form.get('Usuarios').value.splice(index, 1, res);
        this.toastr.success('Se ha modificado el usuario');
      }
    });
  }

  removeUser(item: any, index: number): void {
    this.confirmService.open('Eliminar', '¿Desea eliminar el usuario?').then(res => {
      if (res) {
        this.userService.remove(item.IdUsuario).subscribe(success => {
          if (success) {
            this.form.get('Usuarios').value.splice(index, 1);
          } else {
            this.toastr.error('No se puede eliminar el usuario');
          }
        });
      }
    });
  }

}
