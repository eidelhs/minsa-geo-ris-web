import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ConfirmService } from '@admin-shared/services/confirm.service';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PersonalService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import { ProfileComponent } from '../../components/profile/profile.component';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  keywords: FormControl;
  users: any[];

  constructor(
    private personalService: PersonalService,
    private bsModal: BsModalService,
    private confirmService: ConfirmService,
    private toastr: ToastrService
  ) {
    this.keywords = new FormControl('');
  }

  ngOnInit() {
    this.search();
  }

  search(): void {
    this.personalService.search(this.keywords.value).subscribe(res => {
      this.users = res;
    });
  }

  add(): void {
    this.bsModal.open(ProfileComponent).then(res => {
      if (res) {
        this.search();
      }
    });
  }

  edit(user: any): void {
    this.bsModal.open(ProfileComponent, { user }).then(res => {
      this.search();
    });
  }

  remove(user: any, index: number): void {
    this.confirmService.open('Eliminar', '¿Desea eliminar el personal?').then(res => {
      if (res) {
        this.personalService.remove(user.IdPersonal).subscribe(res => {
          if (res) {
            this.users.splice(index, 1);
          }
        });
      }
    });
  }

}
