import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UcayaliRisComponent } from './ucayali-ris.component';

describe('UcayaliRisComponent', () => {
  let component: UcayaliRisComponent;
  let fixture: ComponentFixture<UcayaliRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UcayaliRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UcayaliRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
