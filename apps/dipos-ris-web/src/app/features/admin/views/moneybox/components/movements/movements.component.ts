import { Component, Input, OnInit } from '@angular/core';
import { MoneyboxService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-movements',
  templateUrl: './movements.component.html',
  styleUrls: ['./movements.component.scss']
})
export class MovementsComponent implements OnInit {

  @Input() reference: string;
  @Input() id_reference: string;

  movements: any[] = [];

  constructor(
    private boxService: MoneyboxService
  ) { }

  ngOnInit() {
    this.boxService.getMovements({ reference: this.reference, id_reference: this.id_reference })
      .subscribe(res => {
        this.movements = res;
      });
  }

}
