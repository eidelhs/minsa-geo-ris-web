import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentsComponent } from './payments.component';
import { PaymentsRoutingModule } from './payments-routing.module';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { PaymentSearchComponent } from './views/search/search.component';
import { PaymentRegisterComponent } from './views/register/register.component';
import { CustomerComponentsModule } from '../../../customers/components/customer-components.module';

@NgModule({
  imports: [
    CommonModule,
    PaymentsRoutingModule,
    AdminSharedModule,
    CustomerComponentsModule
  ],
  declarations: [
    PaymentsComponent,
    PaymentSearchComponent,
    PaymentRegisterComponent
  ]
})
export class PaymentsModule { }
