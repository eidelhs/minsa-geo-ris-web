import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoneyboxComponent } from './moneybox.component';
import { MoneyboxRoutingModule } from './moneybox-routing.module';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { MoneyboxSearchComponent } from './views/search/search.component';
import { OperationsComponent } from './components/operations/operations.component';
import { MovementsComponent } from './components/movements/movements.component';

@NgModule({
  imports: [
    //CommonModule
    MoneyboxRoutingModule,
    AdminSharedModule
  ],
  declarations: [
    MoneyboxComponent,
    MoneyboxSearchComponent,
    OperationsComponent,
    MovementsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MoneyboxModule { }
