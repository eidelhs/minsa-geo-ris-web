import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { MoneyboxService } from '@dipos-ris-web-commons';
import { map } from 'rxjs/operators';
import { OperationsComponent } from '../../components/operations/operations.component';

@Component({
  selector: 'app-moneybox-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class MoneyboxSearchComponent implements OnInit {
  formSearch: FormGroup;
  paginationController: any = {};
  cashes: any[];
  movements: any[] = [];
  isLoading: boolean;

  constructor(
    private moneyboxService: MoneyboxService,
    private fb: FormBuilder,
    private bsModal: BsModalService
  ) {
    this.builder();
  }

  ngOnInit() {
    this.getBoxes();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      id_caja: [null, Validators.required],
      date_op: [new Date(), Validators.required]
    });
  }

  getBoxes(): void {
    this.isLoading = true;
    this.moneyboxService.getAll().subscribe(res => {
      this.formSearch.patchValue({ id_caja: res[0]?.IdCaja });

      this.cashes = res;
      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }

  search(): void {
    if (this.formSearch.invalid) {
      this.formSearch.markAllAsTouched();
      this.formSearch.updateValueAndValidity();
      return;
    }

    const values = this.formSearch.getRawValue();
    values.date_op = values.date_op.toLocaleDateString("es-ES");

    this.isLoading = true;
    this.moneyboxService.getMovements(values)
      .subscribe(res => {
        this.movements = res;
        this.isLoading = false;
      }, () => this.isLoading = false);
  }

  reset(): void {
    this.formSearch.markAsUntouched();
    this.formSearch.reset();
    this.movements = [];
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  open_moneybox() {
    const params = {
      title: 'Aperturar caja',
      status: 1,
      concept_id: 1,
      disabled_amount: true,
      disabled_concepts: true,
      disabled_import: true
    }
    this.bsModal.open(OperationsComponent, { params });
  }

  close_moneybox() {
    const params = {
      title: 'Cerrar caja',
      status: 2,
      concept_id: 2,
      disabled_amount: true,
      disabled_concepts: true,
      disabled_import: true
    }
    this.bsModal.open(OperationsComponent, { params });
  }

  entry_moneybox() {
    const params = {
      title: 'Registrar ingreso',
      type: 'E',
      status: 2
    }
    this.bsModal.open(OperationsComponent, { params });
  }

  egress_moneybox() {
    const params = {
      title: 'Registrar egreso',
      type: 'S',
      status: 2
    }
    this.bsModal.open(OperationsComponent, { params });
  }

}
