import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MoneyboxService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-operations',
  templateUrl: './operations.component.html',
  styleUrls: ['./operations.component.scss']
})
export class OperationsComponent implements OnInit {
  @Input() params: any;
  form: FormGroup;
  boxes: any[] = [];
  concepts: any[] = [];
  currencies: any[] = [];

  constructor(
    private boxService: MoneyboxService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private bsModal: BsModalService
  ) {

  }

  ngOnInit() {
    this.build();
    this.getBoxes();
    this.getConcepts();
    this.getCurrencies();
  }

  build(): void {
    this.form = this.fb.group({
      IdCaja: [null, Validators.required],
      TotalCaja: 0,
      IdConcepto: [{ value: this.params?.concept_id, disabled: this.params?.disabled_concepts }, Validators.required],
      Referencia: this.params?.reference,
      IdReferencia: this.params?.id_reference,
      IdMoneda: 'PEN',
      Simbolo: 'S/',
      Importe: [{ value: this.params?.amount || 0, disabled: this.params?.disabled_import }, [this.validatorGreaterThan(0), Validators.required]],
      Observaciones: null
    });

    // Si son egresos controlamos que el monto no exceda el importe de la caja
    this.form.setValidators(this.validatorEgressImport());

    this.form.get('IdCaja').valueChanges.subscribe(value => {
      const box = this.boxes.find(x => x.IdCaja == value);
      this.form.get('TotalCaja').setValue(box?.Importe || 0);
    });
  }

  validatorEgressImport(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const control = this.form.get('Importe');
      // control.setValidators([this.validatorGreaterThan(0), Validators.required]);

      if (this.params?.type == 'S' && this.form.get('IdCaja').value) {
        const box_total = this.form.get('TotalCaja').value;

        if (+control.value > box_total) {
          control.setErrors({ amountExceed: true });
        }
      }

      return null;
    }
  }

  validatorGreaterThan(n: number): ValidatorFn {
    return (control: FormControl): ValidationErrors => {
      const value = +control.value
      if (isNaN(value)) return { invalid: true };
      else if ((Math.round(value * 100) / 100) <= n) return { greater: true }; // Sólo valores como 0.01 como mínimo

      return null;
    }
  }

  getBoxes(): void {
    this.boxService.getAll(this.params.status).subscribe(res => {
      this.boxes = res;
    });
  }

  getConcepts(): void {
    this.boxService.getConcepts(this.params?.type || '').subscribe(res => {
      this.concepts = res;
    });
  }

  getCurrencies(): void {
    this.boxService.getCurrencies().subscribe(res => {
      this.currencies = res;
    });
  }

  changeMoneybox(): void {
    const total = this.form.get('TotalCaja').value;
    if (!this.params?.type) {
      this.form.get('Importe').setValue(total);
    }
  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
      return;
    }

    const values = this.form.getRawValue();

    this.boxService.movementInsert({ movement: values }).subscribe(res => {
      if (res.Ok) {
        this.toastr.success(res.Mensaje, 'Listo');
        this.bsModal.close(true);
      } else {
        this.toastr.error(res.Mensaje, 'Error');
      }
    });
  }

}
