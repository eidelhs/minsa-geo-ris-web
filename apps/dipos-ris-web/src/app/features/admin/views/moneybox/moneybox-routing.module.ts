import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MoneyboxComponent } from './moneybox.component';
import { MoneyboxSearchComponent } from './views/search/search.component';

const routes: Routes = [
  {
    path: '',
    component: MoneyboxComponent,
    children: [
      {
        path: '',
        component: MoneyboxSearchComponent
      },
      {
        path: 'pagos',
        loadChildren: () => import('./views/payments/payments.module').then(m => m.PaymentsModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MoneyboxRoutingModule { }