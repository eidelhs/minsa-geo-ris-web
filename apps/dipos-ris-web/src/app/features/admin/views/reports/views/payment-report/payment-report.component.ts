import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportsService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-payment-report',
  templateUrl: './payment-report.component.html',
  styleUrls: ['./payment-report.component.scss']
})
export class PaymentReportComponent implements OnInit {
  formSearch: FormGroup;
  paginationController: any = {};
  accounts: any[] = [];
  isLoading: boolean;

  constructor(
    private reportsService: ReportsService,
    private fb: FormBuilder
  ) {
    this.builder();
   }

  ngOnInit() {
  }

  builder(): void {
    this.formSearch = this.fb.group({
      date_start: [new Date(), Validators.required],
      date_end: [new Date(), Validators.required],
      id_state: ''
    });
  }

  search(): void {
    if (this.formSearch.invalid) {
      this.formSearch.markAllAsTouched();
      this.formSearch.updateValueAndValidity();
      return;
    }

    const range = this.formSearch.getRawValue();
    const date_start = range.date_start.toISOString();
    const date_end = range.date_end.toISOString();
    const id_state = range.id_state || null;

    this.isLoading = true;
    this.reportsService.accountsByPay(date_start, date_end, id_state)
      .subscribe(res => {
        this.accounts = res;
        this.isLoading = false;
      }, () => this.isLoading = false);
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }
}
