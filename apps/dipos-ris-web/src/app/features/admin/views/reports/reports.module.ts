import { NgModule } from '@angular/core';
import { ReportsComponent } from './reports.component';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { ReportsRoutingModule } from './report-routing.module';
import { SalesReportComponent } from './views/sales-report/sales-report.component';
import { ProductsReportComponent } from './views/products-report/products-report.component';
import { CustomersReportComponent } from './views/customers-report/customers-report.component';
import { ReceiveReportComponent } from './views/receive-report/receive-report.component';
import { PaymentReportComponent } from './views/payment-report/payment-report.component';
import { IncomesExpensesReportComponent } from './views/incomes-expenses-report/incomes-expenses-report.component';
import { UtilitiesReportComponent } from './views/utilities-report/utilities-report.component';
import { InventoryReportComponent } from './views/inventory-report/inventory-report.component';

@NgModule({
  imports: [
    AdminSharedModule,
    ReportsRoutingModule
  ],
  declarations: [
    ReportsComponent,
    SalesReportComponent,
    ProductsReportComponent,
    CustomersReportComponent,
    ReceiveReportComponent,
    PaymentReportComponent,
    IncomesExpensesReportComponent,
    UtilitiesReportComponent,
    InventoryReportComponent
  ]
})
export class ReportsModule { }
