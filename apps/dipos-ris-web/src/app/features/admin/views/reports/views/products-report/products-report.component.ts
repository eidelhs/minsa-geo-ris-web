import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ReportsService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-products-report',
  templateUrl: './products-report.component.html',
  styleUrls: ['./products-report.component.scss']
})
export class ProductsReportComponent implements OnInit {
  formSearch: FormGroup;
  paginationController: any = {};
  sales: any[] = [];
  isLoading: boolean;

  constructor(
    private reportsService: ReportsService,
    private fb: FormBuilder
  ) {
    this.builder();
   }

  ngOnInit() {
  }

  builder(): void {
    this.formSearch = this.fb.group({
      date_start: [new Date(), Validators.required],
      date_end: [new Date(), Validators.required]
    });
  }

  search(): void {
    if (this.formSearch.invalid) {
      this.formSearch.markAllAsTouched();
      this.formSearch.updateValueAndValidity();
      return;
    }

    const range = this.formSearch.getRawValue();
    const date_start = range.date_start.toISOString();
    const date_end = range.date_end.toISOString();

    this.isLoading = true;
    this.reportsService.salesByProduct(date_start, date_end)
      .subscribe(res => {
        this.sales = res;
        this.isLoading = false;
      }, () => this.isLoading = false);
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }
}
