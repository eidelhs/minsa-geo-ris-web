import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UbicaRisComponent } from './ubica-ris.component';

describe('UbicaRisComponent', () => {
  let component: UbicaRisComponent;
  let fixture: ComponentFixture<UbicaRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UbicaRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UbicaRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
