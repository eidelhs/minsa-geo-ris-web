import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { TrademarkComponent } from '../../components/trademark/trademark.component';
import { UnitAddComponent } from '../../components/unit-add/unit-add.component';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form: FormGroup;

  categories: any[] = [];

  trademark = new FormControl();
  trademarkOptions = {
    //css: 'form-control-sm rounded-pill',
    fn: (text: string) => this.fnTrademarks(text)
  }
  trademarks = [];

  isLoading: boolean;

  @ViewChild('picture', { static: false }) picture: ElementRef;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private productService: ProductService,
    private activateRoute: ActivatedRoute,
    private modal: BsModalService,
    private toastr: ToastrService,
    private sanitization: DomSanitizer
  ) {
    this.build();
  }

  ngOnInit() {
    this.productService.categories().subscribe(res => {
      this.categories = res;
    });

    this.productService.trademarks().subscribe(res => {
      this.trademarks = res;
    });

    this.activateRoute.paramMap.subscribe(params => {
      const id = +params.get('id');

      this.unidades.clear();

      if (id) {
        this.load(id)
      }
    });
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    this.form = this.fb.group({
      IdProducto: null,
      IdCategoria: [null, Validators.required],
      Categoria: null,
      IdMarca: null,
      Marca: null,
      Nombre: [null, Validators.required],
      Descripcion: null,
      IdMoneda: '01SOL',
      CostoPromedio: [null, [Validators.required, onlyNumbers]],
      StockMinimo: [null, onlyNumbers],
      FechaRegistro: null,
      IdPersonalRegistro: null,
      FechaActualizacion: null,
      IdPersonalActualiza: null,
      FlagInafectoIGV: false,
      TipoImpuesto: '1000',
      FlagISC: false,
      FlagICBPER: false,
      FlagStock: true,
      FlagSerie: false,
      FlagActivo: true,
      UnidadesMedida: this.fb.array([]),

      //Variables auxiliares
      // trademark: null,
      //Manejo de foto
      file: null,
      image: null,
    });

    // Para actualizar los campos de marca
    this.trademark.valueChanges
      .subscribe(val => {
        this.form.patchValue({
          IdMarca: val?.value || null,
          Marca: val?.label || null
        })
      });
  }

  load(id: number): void {
    this.isLoading = true;

    this.productService.getItem(id).subscribe(res => {
      this.form.patchValue({
        ...res,
        TipoImpuesto: res.FlagInafectoIGV ? '9998' : '1000' //Modificar en ficha de producto
      });

      this.trademark.setValue({ value: res.IdMarca, label: res.Marca });

      res.UnidadesMedida.forEach((u: any) => {
        this.unidades.push(this.fb.group(u));
      });

      this.isLoading = false;
    }, () => {
      this.isLoading = false;
    });
  }

  get unidades(): FormArray {
    return this.form.get('UnidadesMedida') as FormArray;
  }

  get image(): string {
    return this.form.get('image').value;
  }

  uploadPicture(): void {
    this.picture.nativeElement.click();
  }

  setFile(file: File): void {
    this.form.get('file').setValue(file);

    const reader = new FileReader();
    reader.onload = (e) => {
      const result = this.sanitization.bypassSecurityTrustUrl(e.target.result as string);
      this.form.get('image').setValue(result);
    }

    reader.readAsDataURL(file);
  }

  changePicture(event: any): void {
    const file = event.target.files[0];
    this.setFile(file);
  }

  dragImage(event): void {
    event.preventDefault();
    event.stopPropagation();
  }

  dropImage(event): void {
    let dataTransfer = event.dataTransfer;
    if (dataTransfer?.files.length) {
      const file = dataTransfer.files[0];
      this.setFile(file);
    }

    event.preventDefault();
    event.stopPropagation();
  }

  errorImage(event: any): void {
    event.onerror = null;
    this.form.get('image').reset();
  }

  selectUndBase(item: any): void {
    this.unidades.controls.forEach(x => x.get('FlagBase').setValue(false));
    item.get('FlagBase').setValue(true);
  }

  open(): void {
    const und = this.unidades.value.find(u => u.FlagBase);

    this.modal.open(UnitAddComponent, { unitBase: und?.IdUnidadMedida }).then(res => {
      if (res) {
        this.unidades.push(this.fb.group(res));
      }
    });
  }

  editUnit(item: any, index: number): void {
    const und = this.unidades.value.find(u => u.FlagBase);

    this.modal.open(UnitAddComponent, { unitBase: und?.IdUnidadMedida, values: item.value }).then(res => {
      if (res) {
        this.unidades.removeAt(index);
        this.unidades.insert(index, this.fb.group(res));
      }
    });
  }

  removeUnit(index: number): void {
    this.unidades.removeAt(index);
  }

  get product() {
    return this.form.getRawValue();
  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
      return;
    }

    const values = this.product;

    console.log(values);
    if (!values.UnidadesMedida.length) {
      this.toastr.warning('Ingrese una presentación del producto como mínimo');
    }

    values.IdProducto = values.IdProducto || 0;
    values.Nombre = values.Nombre.toUpperCase();
    values.PrecioMinimo = values.PrecioMinimo || 0;
    values.StockMinimo = values.StockMinimo || 0;
    values.FechaRegistro = values.FechaRegistro || new Date();
    values.IdPersonalRegistro = 0;
    values.FlagInafectoIGV = values.TipoImpuesto != '1000';

    values.ProductoUnidadMedida = values.UnidadesMedida.map((item: any, index: number) => {
      item.IdProducto = values.IdProducto;
      item.Orden = index + 1;
      item.PrecioMin = item.PrecioMin || 0;
      return item;
    });

    delete values.UnidadesMedida;

    this.isLoading = true;

    this.productService.insert(values).subscribe(res => {
      this.isLoading = false;

      this.form.get('IdProducto').setValue(res);

      /* const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Signed in successfully'
      }); */

      Swal.fire({
        title: 'Guardado',
        text: 'Los datos han sido registrados exitosamente',
        confirmButtonText: 'Aceptar',
        icon: 'success'
      });

      // this.toastr.success('Los datos han sido registrados exitosamente');
    }, () => {
      this.isLoading = false;
      this.toastr.error('Ha ocurrido un error al guardar el item de venta');
    });
  }

  errorInAverageCost(): boolean {
    const price = +this.unidades.value.find(x => x.FlagBase)?.Precio || 0;
    const averageCost = +this.form.get('CostoPromedio').value || 0;

    return this.unidades.controls.length && (averageCost > price);
  }

  reset(): void {
    const values = this.form.value;

    this.form.reset({
      IdMoneda: values.IdMoneda,
      TipoImpuesto: values.TipoImpuesto,
      IdCategoria: values.IdCategoria,
      FlagActivo: true,
      FlagStock: values.FlagStock
    });

    this.unidades.clear();

    this.router.navigateByUrl('/inventario/registro');
  }

  fnTrademarks(text: string) {
    return this.productService.trademarks();
  }

  addMark(): void {
    this.modal.open(TrademarkComponent).then(res => {
      if (res) {
        this.trademark.setValue({ value: res.IdMarca, label: res.Detalle });
        // Agregamos a la variable de  marcas
        this.trademarks.push({ IdMarca: res.IdMarca, Detalle: res.Detalle });
      }
    });
  }

  editMark(): void {
    const data = this.trademark.value;
    this.modal.open(TrademarkComponent, { data: { IdMarca: data.value, Detalle: data.label } }).then(res => {
      if (res) {
        this.trademark.setValue({ value: res.IdMarca, label: res.Detalle });
        // Modificamos en la variable de marcas
        const index = this.trademarks.findIndex(x => x.IdMarca == res.IdMarca)
        this.trademarks.splice(index, 1, { IdMarca: res.IdMarca, Detalle: res.Detalle });
      }
    });
  }

}
