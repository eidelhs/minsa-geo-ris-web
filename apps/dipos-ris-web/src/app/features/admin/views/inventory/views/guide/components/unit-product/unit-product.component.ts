import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-unit-product',
  templateUrl: './unit-product.component.html',
  styleUrls: ['./unit-product.component.scss']
})
export class UnitProductComponent implements OnInit {

  @Input() product: any;
  form: FormGroup;
  units: any[] = [];

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private bsModal: BsModalService
  ) {
    this.build();
  }

  ngOnInit() {
    this.productService.unitsByProduct(this.product.IdProducto).subscribe(res => {
      this.units = res;
      this.form.get('IdUnidadMedida').setValue(res[0]?.IdUnidadMedida);
    });
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    this.form = this.fb.group({
      Cantidad: [1, [Validators.required, onlyNumbers]],
      IdUnidadMedida: [Validators.required],
      Costo: [null, [Validators.required, onlyNumbers]]
    });
  }

  send(): void {
    const values = this.form.getRawValue();
    const unity = this.units.find(item => item.IdUnidadMedida == values.IdUnidadMedida);
    const qty = Math.round(values.Cantidad * unity.Factor * 1000) / 1000;

    let cost = 0;

    if (qty) {
      cost = Math.round((values.Costo / qty) * 100) / 100;
    }

    values.Costo = cost;
    values.Cantidad = qty;
    values.IdUnidadMedidaBase = this.product.IdUnidadMedida;

    this.bsModal.close(values);
  }

}
