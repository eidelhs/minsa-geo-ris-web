import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ProductService, WarehouseService } from '@dipos-ris-web-commons';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss']
})
export class TransferComponent implements OnInit {
  @Input() product: any;

  form: FormGroup;
  whSource: any[] = [];
  whTarget: any[] = [];
  units: any[] = [];

  constructor(
    private fb: FormBuilder,
    private warehouseService: WarehouseService,
    private productService: ProductService,
    private bsModal: BsModalService
  ) {
    this.build();
  }

  ngOnInit() {
    const stock_base = Math.round(this.product.Stock * 100) / 100;
    this.form.get('Stock').setValue(stock_base);
    this.form.get('IdUnidadMedida').setValue(this.product.IdUnidadMedida);

    this.productService.unitsByProduct(this.product.IdProducto).subscribe(res => {
      this.units = res;
    });

    this.warehouseService.getAll().subscribe(res => {
      this.whSource = res;
      this.form.get('IdSource').setValue(res[0]?.IdAlmacen);
      this.form.get('IdTarget').setValue(res[1]?.IdAlmacen);
    });

    this.form.get('IdSource').valueChanges.subscribe(value => {
      this.whTarget = this.whSource.filter(x => x.IdAlmacen != value);
    });
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    this.form = this.fb.group({
      IdSource: [{ value: null, disabled: true }, Validators.required],
      IdTarget: [null, Validators.required],
      Stock: [{ value: 0, disabled: true }],
      IdUnidadMedida: [{ value: null, disabled: true }, Validators.required],
      Quantity: [1, [Validators.required, onlyNumbers]]
    });

    this.form.addValidators(this.fnValidatorQty());
  }

  fnValidatorQty(): ValidatorFn {
    return (group: FormGroup): { [key: string]: any } | null => {
      const stock = group.get('Stock').value;
      const qty = group.get('Quantity').value;
      if (qty > stock) {
        group.get('Quantity').setErrors({ qtyExceed: true });
      }

      return null;
    }
  }

  transfer(): void {
    const values = this.form.getRawValue();

    const source = {
      IdAlmacen: values.IdSource,
      IdProducto: this.product.IdProducto,
      IdUnidadMedida: this.product.IdUnidadMedida,
      Cantidad: this.product.Stock
    }

    const target = {
      IdAlmacen: values.IdTarget,
      IdProducto: this.product.IdProducto,
      IdUnidadMedida: this.product.IdUnidadMedida,
      Cantidad: values.Quantity
    }

    this.warehouseService.transfer(source, target).subscribe(res => {
      if (res.Ok) {
        Swal.fire({
          title: 'Transferido',
          text: 'El stock ha sido transferido exitosamente',
          confirmButtonText: 'Aceptar',
          icon: 'success'
        });

        this.bsModal.close(res);
      } else {
        Swal.fire({
          title: 'Atención',
          text: res.Mensaje,
          confirmButtonText: 'Aceptar',
          icon: 'error'
        });
      }
    });
  }

}
