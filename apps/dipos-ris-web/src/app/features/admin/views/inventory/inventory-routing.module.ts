import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RolGuard } from "@dipos-ris-web-commons";
import { UserRol } from "@dipos-ris-web/models";
import { InventoryComponent } from "./inventory.component";
import { ProductsComponent } from "./views/products/products.component";
import { RegisterComponent } from "./views/register/register.component";

const routes: Routes = [
  {
    path: '',
    component: InventoryComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'productos' },
      {
        path: 'productos',
        component: ProductsComponent
      },
      {
        path: 'registro',
        component: RegisterComponent,
        canActivate: [RolGuard],
        data: { rols: [UserRol.admin] },
      },
      {
        path: 'registro/:id',
        component: RegisterComponent,
        canActivate: [RolGuard],
        data: { rols: [UserRol.admin] },
      },
      {
        path: 'guia-interna',
        loadChildren: () => import('./views/guide/guide.module').then(m => m.GuideModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
