import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { UnitProductComponent } from '../unit-product/unit-product.component';

@Component({
  selector: 'app-guide-item',
  templateUrl: './guide-item.component.html',
  styleUrls: ['./guide-item.component.scss']
})
export class GuideItemComponent implements OnInit {
  @Input() queue: BehaviorSubject<any>;

  keywords: FormControl = new FormControl();
  products = [];
  paginatorController: any = {};
  form: FormGroup;

  constructor(
    private productsService: ProductService,
    private toastr: ToastrService,
    private fb: FormBuilder,
    private bsModal: BsModalService,
    private cdRef: ChangeDetectorRef
  ) {

  }

  ngOnInit() {
  }

  search(): void {
    const keywords = this.keywords.value;
    if (/[0-9]/g.test(keywords)) {
      this.keywords.reset();
    }

    this.productsService.search({ keywords, sort: 'nombre' }).subscribe(res => {
      this.products = res;
      if (res.length === 1) {
        this.selectProduct(res[0]);
      }
    });
  }

  selectProduct(item: any): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    const product = {
      ...item,
      CostoPromedio: item.CostoPromedio,
      CostoAdquisicion: [item.CostoPromedio, onlyNumbers],
      CostoPromedioActual: item.CostoPromedio,
      Precio: [item.Precio, onlyNumbers],
      Cantidad: [1, onlyNumbers],
      Importe: item.Precio
    };

    // Creamos el FormGroup
    this.form = this.fb.group(product);

    this.form.valueChanges.subscribe(values => {
      const price = +values.Precio;
      const qty = +values.Cantidad;

      // Calculamos el importe
      const importe = Math.round(price * qty * 100) / 100;
      this.form.get('Importe').setValue(importe, { emitEvent: false });

      // Costo promedio
      const averageCost = +values.CostoPromedio;
      const unitCost = +values.CostoAdquisicion;

      // var promedio = Math.Round((values.Stock * averageCost + qty * unitCost) / (values.Stock + qty), 2);
      const cost = Math.round(((values.Stock * averageCost + qty * unitCost) / (values.Stock + qty)) * 100) / 100;
      this.form.get('CostoPromedioActual').setValue(cost, { emitEvent: false });
    });

    this.form.setValidators(this.fnValidations());
  }

  fnValidations(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const cost = +group.get('CostoPromedioActual').value;
      const price = group.get('Precio');

      if (isNaN(+price.value)) {
        price.setErrors({ invalid: true });
      } else if (cost > +price.value) {
        price.setErrors({ price: true });
      } else {
        price.setErrors(null);
      }

      return null;
    };
  }

  addToList(): void {
    const product = this.form.getRawValue();
    product.Costo = product.CostoPromedioActual;

    this.queue.next(product);
    this.toastr.success(`Se agregó ${product.Nombre} a la lista`);
  }

  openModal(): void {
    this.bsModal.open(UnitProductComponent, { product: this.form.getRawValue() }).then(res => {
      if (res) {
        console.log(res);
        this.form.patchValue({
          Cantidad: res.Cantidad,
          CostoAdquisicion: res.Costo
        });
      }
    });
  }

  changePagination(event): void {
    this.paginatorController = event;
    this.cdRef.detectChanges();
  }

}
