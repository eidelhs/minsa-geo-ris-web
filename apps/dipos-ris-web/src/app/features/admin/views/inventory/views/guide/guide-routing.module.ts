import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { InternalGuideComponent } from "./views/internal-guide/internal-guide.component";

const routes: Routes = [
  {
    path: '',
    component: InternalGuideComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }