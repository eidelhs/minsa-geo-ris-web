import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';

@Component({
  selector: 'app-unit-add',
  templateUrl: './unit-add.component.html',
  styleUrls: ['./unit-add.component.scss']
})
export class UnitAddComponent implements OnInit {

  @Input() unitBase: string | null;

  @Input() values?: any;

  form: FormGroup;

  units: any[] = [];

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private modal: BsModalService
  ) { }

  ngOnInit() {
    this.build();

    this.productService.units().subscribe(res => {
      this.units = res;
    });
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

    this.form = this.fb.group({
      IdProducto: null,
      EAN: null,
      FlagBase: false,
      FlagVenta: true,
      IdUnidadMedida: ['UND', Validators.required],
      UnidadMedida: null,
      Factor: [1, [Validators.required, onlyNumbers]],
      Orden: 1,
      Precio: [null, [Validators.required, onlyNumbers]],
      PrecioMin: [null, [onlyNumbers]],
    });

    if (this.values) {
      this.form.patchValue(this.values);
    }

  }

  save(): void {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.form.updateValueAndValidity();
      return;
    }

    const values = this.form.value;
    values.FlagBase = values.FlagBase || !this.unitBase;
    values.UnidadMedida = this.units.find(u => u.IdUnidadMedida == values.IdUnidadMedida)?.Detalle;
    this.modal.close(values);
  }

  reset(): void {
    this.form.reset({
      IdUnidadMedida: 'UND',
      Factor: 1,
      FlagVenta: true,
    });
  }

}
