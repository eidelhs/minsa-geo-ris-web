/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { StocktackingComponent } from './stocktacking.component';

describe('StocktackingComponent', () => {
  let component: StocktackingComponent;
  let fixture: ComponentFixture<StocktackingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StocktackingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocktackingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
