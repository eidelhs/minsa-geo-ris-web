import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipoMultidiciplinarioComponent } from './equipo-multidiciplinario.component';

describe('EquipoMultidiciplinarioComponent', () => {
  let component: EquipoMultidiciplinarioComponent;
  let fixture: ComponentFixture<EquipoMultidiciplinarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipoMultidiciplinarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipoMultidiciplinarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
