import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductService } from '../../../../../../../../libs/dipos-ris-web-commons/src/lib/http/product.service';

@Component({
  selector: 'app-equipo-multidiciplinario',
  templateUrl: './equipo-multidiciplinario.component.html',
  styleUrls: ['./equipo-multidiciplinario.component.scss']
})
export class EquipoMultidiciplinarioComponent implements OnInit {

  formSearch: FormGroup;
  form: FormGroup;
  quick_label: String = 'Ingrese el establecimiento de salud:';
  ris = new FormControl();
  esris = [];
  paginationController: any = {};
  orders: any[] = [];
  isLoading: boolean;
  constructor(
    private fb: FormBuilder,
    private productService: ProductService) {
    this.builder();
  }

  ngOnInit() {
    this.formSearch.get('quick').setValue(1);
    this.productService.trademarks().subscribe(res => {
      this.esris = res;
    });
    // this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      option: 1,
      quick: null,
      ris: null
    });

    this.form = this.fb.group({
      id_order: null,
      client: null,
      id_state: null,
      id_employee: null,
      employee: null,
      date_start: null,
      date_end: null
    });
  }

  onChange($event){
    const valor = $event.target.value;
    if(valor == '1') {
      this.quick_label = 'Establecimiento de Salud:';
    }else if(valor == '2') {
      this.quick_label = 'Personal de Salud:';
    }
    this.productService.trademarks().subscribe(res => {
      this.esris = res;
    });
  }

  search() {
    console.log(this.formSearch.get('ris').value);
    /*
    this.productService.resultados().subscribe(res => {
      this.orders = res;
    });*/
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

}
