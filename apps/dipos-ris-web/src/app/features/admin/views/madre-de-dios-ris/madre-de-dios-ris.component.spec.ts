import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MadreDeDiosRisComponent } from './madre-de-dios-ris.component';

describe('MadreDeDiosRisComponent', () => {
  let component: MadreDeDiosRisComponent;
  let fixture: ComponentFixture<MadreDeDiosRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MadreDeDiosRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MadreDeDiosRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
