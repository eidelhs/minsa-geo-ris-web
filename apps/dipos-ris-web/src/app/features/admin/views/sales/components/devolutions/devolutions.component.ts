import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { MoneyboxService, OrderService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-devolutions',
  templateUrl: './devolutions.component.html',
  styleUrls: ['./devolutions.component.scss']
})
export class DevolutionsComponent implements OnInit {

  @Input() order: any;
  form: FormGroup;
  boxes = [];

  constructor(
    private boxService: MoneyboxService,
    private orderService: OrderService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private bsModal: BsModalService
  ) { }

  ngOnInit() {
    this.build();
    this.getBoxes();
    this.getMovements();
  }

  build(): void {
    const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);
    const order = {
      ...this.order,
      MontoPagado: 0,
      IdCaja: [null, Validators.required],
      TotalCalculado: this.order.TotalVenta,
      MontoDevolver: 0
    };

    order.PedidoDetalle = this.fb.array(
      order.PedidoDetalle.map(item => {
        const group = this.fb.group({
          ...item,
          Devolver: [0, [onlyNumbers]]
        });

        group.setValidators(this.fnValidations());

        return group;
      })
    );

    this.form = this.fb.group(order);

    this.calculateTotal();
  }

  // Validacion de cantidad a devolver que no sea mayor a cantidad del pedido
  fnValidations(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const qty = group.get('Cantidad').value;
      const devolution = group.get('Devolver');
      if (devolution.value > qty) {
        devolution.setErrors({ max: true });
      }
      // else {
      //   devolution.setErrors(null);
      // }

      return null;
    };
  }

  getBoxes(): void {
    this.boxService.getAll(2).subscribe(res => {
      this.boxes = res;

      this.form.get('IdCaja').setValue(res[0]?.IdCaja);
    });
  }

  getMovements(): void {
    this.boxService.getMovements({ reference: 'PEDIDO', id_reference: this.order.IdPedido }).subscribe(res => {
      const total = Math.round(res.reduce((a, m) => a + m.Importe * (m.TipoMovimiento == 'E' ? 1 : -1), 0) * 100) / 100;
      this.form.get('MontoPagado').setValue(total);
    });
  }

  get products(): FormArray {
    return this.form.get('PedidoDetalle') as FormArray;
  }

  calculateTotal(): void {
    this.products.valueChanges.subscribe(changes => {
      let subtotal = 0;
      this.products.controls.forEach((group, index: number) => {
        const qty = group.get('Cantidad').value - +group.get('Devolver').value;
        const price = group.get('Precio').value;

        subtotal += Math.round(qty * price * 100) / 100;
        group.get('Importe').setValue(subtotal, { emitEvent: false });
      });

      if (!isNaN(subtotal)) {
        this.form.get('TotalCalculado').setValue(subtotal);
        const devolver = Math.round((this.form.get('TotalAjuste').value - subtotal) * 100) / 100;
        const pagado = this.form.get('MontoPagado').value;
        this.form.get('MontoDevolver').setValue(devolver > pagado ? pagado : devolver);
      }
    });
  }

  cloneValue(item): void {
    item.get('Devolver').setValue(item.get('Cantidad').value);
  }

  save(): void {
    const values = this.form.getRawValue();

    if (!values.TotalCalculado) {
      this.toastr.warning('Si está devolviendo toda la mercadería es mejor que anule el pedido');
      return;
    }

    const saldo = this.boxes.find(x => x.IdCaja == values.IdCaja)?.Importe || 0;

    if (values.TotalCalculado > saldo) {
      this.toastr.warning('No hay suficientes fondos para desembolsar de la caja');
      return;
    }

    if (values.MontoDevolver > values.MontoPagado) {
      this.toastr.warning('El monto a devolver supera el monto pagado por el cliente');
      return;
    }

    const products = values.PedidoDetalle
      .filter(item => +item.Devolver)
      .map(item => {
        item.Cantidad = Math.round((item.Cantidad - item.Devolver) * 1000) / 1000;
        return item;
      });

    const movements = products.map(item => ({
      IdAlmacen: item.IdAlmacen,
      TipoMovimiento: "E",
      IdMotivo: 8, //DEVOLUCION
      Fecha: new Date(),
      IdProducto: item.IdProducto,
      Cantidad: +item.Devolver,
      IdUnidadMedida: item.IdUnidadMedida,
      IdMoneda: values.IdMoneda,
      Costo: item.CostoUndBase,
      Precio: item.Precio,
      Observaciones: `PEDIDO N° ${values.IdPedido.toString().padStart(7, '0')}`
    }));

    this.orderService.devolution(values, movements, values.IdCaja, values.MontoDevolver).subscribe(res => {
      values.Saldo = Math.round((values.Saldo - res.MontoDevolver) * 100) / 100;
      this.bsModal.close({ order: values, devolutions: products });
    });
  }

}
