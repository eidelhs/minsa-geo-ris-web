import { Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ProductService } from '@dipos-ris-web-commons';
import { of, throwError } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, filter, switchMap, take, tap } from 'rxjs/operators';

@Component({
  selector: 'app-products-search',
  templateUrl: './products-search.component.html',
  styleUrls: ['./products-search.component.scss']
})
export class ProductsSearchComponent implements OnInit, OnDestroy {

  @Input() almacenId: FormControl;

  @Input() keywords: FormControl;

  @Output() selected = new EventEmitter<any>();

  @Output() close = new EventEmitter<boolean>();

  textForm: FormControl = new FormControl();

  @Input() results: any[] = [];

  sizePage: number = 12;

  page: number = 1;

  pages: number = 0;

  total: number = 0;

  isLoading: boolean;

  isClosing: boolean;

  @ViewChild('perfectScroll', { static: false }) perfectScroll: ElementRef;

  // @ViewChild('inputSearch', { static: false }) inputSearch: ElementRef;

  constructor(
    private productService: ProductService
  ) {

  }

  ngOnInit() {
    this.keywords.valueChanges.subscribe(text => {
      this.textForm.setValue(text);

      // setTimeout(() => {
      //   this.inputSearch.nativeElement.focus();
      // }, 0);
    });


    this.watchKeywords();
    this.textForm.setValue(this.keywords.value);

    document.querySelector('body').classList.add('overflow-hidden');
  }

  ngOnDestroy(): void {
    document.querySelector('body').classList.remove('overflow-hidden');
  }

  watchKeywords(): void {
    this.textForm.valueChanges
      .pipe(
        tap(value => {
          this.page = 1;

          if (!value) {
            this.results = [];
            this.total = 0;
          }
        }),
        filter((value: string) => value?.length > 1),
        distinctUntilChanged(),
        tap(() => this.isLoading = true),
        debounceTime(400),
        // switchMap((values: any) => this.productService.search(values))
        switchMap((value: any) => {
          const params = {
            keywords: value,
            page: this.page,
            size: this.sizePage,
            almacenId: this.almacenId.value,
            flagStock: true
          };

          return this.productService.paging(params)
            .pipe(
              catchError(() => {
                this.isLoading = false;
                this.results = [];
                this.total = 0;
                this.pages = 0;
                return of();
              })
            )
        })
      )
      .subscribe((res: any) => {
        this.isLoading = false;
        if (res) {
          this.total = res.total;
          this.pages = res.pages;
          this.results = res.data;
        }

        this.perfectScroll.nativeElement.scrollTop = 0;
      });
  }

  track(event: Event) {
    if (!this.isLoading && this.page < this.pages) {
      const params = {
        keywords: this.textForm.value,
        page: ++this.page,
        size: this.sizePage,
        almacenId: this.almacenId.value,
        flagStock: true
      }

      this.isLoading = true;

      this.productService.paging(params)
        .subscribe((res: any) => {
          this.isLoading = false;
          // this.total = res.total;
          // this.pages = res.pages;
          this.results = [...this.results, ...res.data];
        }, () => {
          this.isLoading = false;
        });
    }
  }

  trackById(index: number, item: any) {
    return item.IdProducto;
  }

  select(product: any, unit: any): void {
    const p = {
      ...product,
      IdAlmacen: unit.IdAlmacen,
      IdUnidadMedida: unit.IdUnidadMedida,
      Factor: unit.Factor,
      Precio: unit.Precio,
    }

    this.selected.emit(p);
  }

  closeSearch(): void {
    this.isClosing = true;
    setTimeout(() => {
      this.close.emit(true);
    }, 300);
  }

  loadImage(event: any): void {
    event.path[1].classList.remove('img-loading');
  }

  errorImage(event: any): void {
    event.onerror = null;
    event.target.src = './assets/no-photos.png';
  }

}
