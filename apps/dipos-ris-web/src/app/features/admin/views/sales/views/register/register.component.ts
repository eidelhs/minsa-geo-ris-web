import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { ConfirmService } from '@admin-shared/services/confirm.service';
import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { MoneyboxService, OrderService, ProductService, RenderComponentService, SessionService, WarehouseService } from '@dipos-ris-web-commons';
import { ToastrService } from 'ngx-toastr';
import { CustomerListComponent } from '../../../customers/components/customer-list/customer-list.component';
import { DevolutionsComponent } from '../../components/devolutions/devolutions.component';
import { InvalidationComponent } from '../../components/invalidation/invalidation.component';
import { MovementsComponent } from '../../../moneybox/components/movements/movements.component';
import { PaymentComponent } from '../../components/payment/payment.component';
import { TicketComponent } from '../../components/ticket/ticket.component';

@Component({
  selector: 'app-sales-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class SalesRegisterComponent implements OnInit {
  form: FormGroup;
  formSearch: FormGroup;
  textSearch: FormControl = new FormControl();
  warehouses: any[] = [];
  sticky: any;
  timeSticky: any;
  searchOpen: boolean;
  isLoadingAttend: boolean;

  constructor(
    private session: SessionService,
    private fb: FormBuilder,
    private warehouse: WarehouseService,
    // private productService: ProductService,
    private orderService: OrderService,
    private bsModal: BsModalService,
    private confirmService: ConfirmService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private location: Location,
    private render: RenderComponentService,
    private sanitizer: DomSanitizer
  ) {
    this.build();
  }

  ngOnInit() {
    this.watchTotals();
    this.watchAjustes();

    this.getWarehouses();
    this.load();

    this.form.get('IdEstado').valueChanges.subscribe(res => {
      let state = '';
      switch (res) {
        case 1: state = 'REGISTRADO'; break;
        case 2: state = 'ATENDIDO'; break;
        case 3: state = 'ANULADO'; break;
      }

      this.form.get('Estado').setValue(state, { emitEvent: false });
    });
  }

  build(): void {
    this.form = this.fb.group({
      IdPedido: null,
      IdCliente: null,
      ClienteTipoDocumento: null,
      ClienteNroDocumento: null,
      RazonSocial: [{ value: null, disabled: true }],
      ClienteDireccion: [{ value: null, disabled: true }],
      IdFormaPago: 1,
      FormaPago: null,
      FechaPago: new Date(),
      IdMoneda: 'PEN',
      Moneda: 'S/',
      TotalBruto: 0,
      TotalDescuento: 0,
      TotalImpuesto: 0,
      TotalInafecto: 0,
      TotalExonerado: 0,
      TotalVenta: 0,
      MontoAjuste: 0,
      TotalAjuste: 0,
      FlagAjustes: true,
      Saldo: 0,
      FechaRegistro: null,
      IdPersonalRegistro: null,
      PersonalRegistro: null,
      FechaAnulacion: null,
      IdPersonalAnulacion: null,
      Observaciones: null,
      IdEstado: null,
      Estado: null,
      PedidoDetalle: this.fb.array([], Validators.required)
    });

    this.formSearch = this.fb.group({
      IdAlmacen: null,
      keywords: null
    });
  }

  getWarehouses(): void {
    this.warehouse.getAll().subscribe(res => {
      this.warehouses = res;
      this.formSearch.get('IdAlmacen').setValue(res[0]?.IdAlmacen);
    });
  }

  load(): void {
    const id = +this.activatedRoute.snapshot.paramMap.get('id');
    if (id) {
      this.orderService.getById(id).subscribe(res => {
        this.form.patchValue(res);

        res.PedidoDetalle.forEach(item => this.addItem(item));
      });
    }
  }

  watchTotals(): void {
    this.products.valueChanges.subscribe(changes => {
      let total = 0, inafecto = 0, tasa_impuesto = 0.18;

      // Importes de la tabla de productos
      this.products.controls.forEach((control, index: number) => {
        const qty = control.get('Cantidad').value,
          price = control.get('Precio').value;

        const subtotal = qty * price;

        if (control.get('FlagInafectoIGV').value) {
          inafecto += subtotal;
        }

        total += subtotal;

        control.get('Importe').setValue(subtotal, { emitEvent: false });
        control.get('Item').setValue(index + 1, { emitEvent: false });
      });

      // Calculamos totales
      total = Math.round(total * 100) / 100;
      inafecto = Math.round(inafecto * 100) / 100;

      this.form.get('TotalVenta').setValue(total);
      this.form.get('TotalInafecto').setValue(inafecto);

      const totalBruto = Math.round((total - inafecto) * 100 / (1 + tasa_impuesto)) / 100;
      this.form.get('TotalBruto').setValue(totalBruto);
      this.form.get('TotalImpuesto').setValue(Math.round(totalBruto * tasa_impuesto * 100) / 100);

      const ajuste = Math.round(total * 10) / 10 - total;
      const cobrar = total + (this.form.get('FlagAjustes').value ? ajuste : 0);
      this.form.get('MontoAjuste').setValue(ajuste);
      this.form.get('TotalAjuste').setValue(cobrar);
    });
  }

  watchAjustes(): void {
    this.form.get('FlagAjustes').valueChanges.subscribe(active => {
      const ajuste = this.form.get('MontoAjuste').value;
      const total = this.form.get('TotalVenta').value;
      this.form.get('TotalAjuste').setValue(total + (active ? ajuste : 0));
    });
  }

  get products(): FormArray {
    return this.form.get('PedidoDetalle') as FormArray;
  }

  get keywords(): FormControl {
    return this.formSearch.get('keywords') as FormControl;
  }

  get almacenId(): FormControl {
    return this.formSearch.get('IdAlmacen') as FormControl;
  }

  getWeight(item: any): boolean {
    return ['KGR', 'GRM'].includes(item.get('IdUnidadMedida').value);
  }

  errorImage(event: any): void {
    event.onerror = null;
    event.target.src = './assets/no-photos.png';
  }

  incrementQty(control: any) {
    let value = +control.get('Cantidad').value + 1;
    control.get('Cantidad').setValue(value);
  }

  decrementQty(control: any) {
    let value = +control.get('Cantidad').value;
    if (value > 1) value--;
    control.get('Cantidad').setValue(value);
  }

  productSearch(): void {
    this.warehouse.stock({ id_warehouse: this.almacenId.value, keywords: this.keywords.value })
      .subscribe(res => {
        if (res.length == 1) {
          const item = {
            ...res[0],
            IdAlmacen: this.almacenId.value
          };

          this.addItem(item);
          this.showSticky(item);
          this.searchOpen = false;
        } else if (res.length > 1) {
          this.textSearch.setValue(this.keywords.value);
          this.searchOpen = true;
        } else {
          this.toastr.error('El producto no existe o no hay existencias', 'Atención');
          this.searchOpen = false;
        }

        this.keywords.reset();
      });
  }

  selectProduct(item: any): void {
    // this.searchOpen = false;    
    this.addItem(item);
    this.showSticky(item);
  }

  showSticky(item: any): void {
    if (this.timeSticky) {
      clearTimeout(this.timeSticky);
    }

    this.sticky = item;
    this.timeSticky = setTimeout(() => {
      this.sticky = null;
    }, 5000);
  }

  addItem(values?: any): void {
    const obj = this.products.controls.find(x => x.get('IdProducto').value == values.IdProducto);

    if (obj) {
      const qty = obj.get('Cantidad');
      qty.setValue(+qty.value + 1);
    } else {
      const onlyNumbers = Validators.pattern(/^[0-9]\d*(\.\d+)?$/);

      const item = {
        IdPedido: values?.IdPedido,
        Item: values?.Item,
        Imagen: values?.image,
        IdProducto: values?.IdProducto,
        Producto: values?.Producto || values?.Nombre,
        Categoria: values?.Categoria,
        IdAlmacen: values?.IdAlmacen,
        // Almacen: values?.Almacen,
        Detalle: values?.Detalle,
        Cantidad: [values?.Cantidad || 1, [Validators.required, onlyNumbers]],
        Stock: [values?.Stock],
        IdUnidadMedida: values?.IdUnidadMedida,
        UnidadMedida: values?.UnidadMedida,
        Factor: values?.Factor || 1,
        CostoUndBase: values?.CostoUndBase || values?.CostoPromedio,
        Precio: [values?.Precio, [Validators.required, onlyNumbers]],
        PrecioMin: values?.PrecioMin,
        Descuento: values?.Descuento || 0,
        TasaImpuesto: values?.TasaImpuesto || 0.18,
        Impuesto: values?.Impuesto || 0,
        Importe: values?.Importe || 0,
        FlagStock: values?.FlagStock,
        FlagInafectoIGV: values?.FlagInafectoIGV
      };

      const group = this.fb.group(item);
      // Validación de stock
      group.setValidators(this.fnValidations());

      this.products.push(group);
    }

  }

  fnValidations(): ValidatorFn {
    return (group: FormGroup): ValidationErrors => {
      const qty = group.get('Cantidad');
      const stock = group.get('Stock').value;
      const flagStock = group.get('FlagStock').value;
      if (flagStock && typeof stock === 'number' && stock < qty.value) {
        qty.setErrors({ inventory: true });
      } else {
        qty.setErrors(null);
      }

      return null;
    };
  }

  removeItem(item: any, index: number): void {
    this.products.removeAt(index);
  }

  addClient(): void {
    this.bsModal.open(CustomerListComponent).then(res => {
      if (res) {
        this.form.patchValue({
          IdCliente: res.IdPropietario,
          ClienteTipoDocumento: res.TipoDocumento,
          ClienteNroDocumento: res.NroDocumento,
          RazonSocial: res.RazonSocial,
          ClienteDireccion: res.Direccion
        })
      }
    });
  }

  save(): void {
    const order = this.form.getRawValue();
    order.IdEstado = order.IdEstado || 1;

    this.orderService.register(order, order.PedidoDetalle).subscribe(res => {
      this.form.patchValue({
        IdPedido: res.order.IdPedido,
        IdEstado: res.order.IdEstado,
        Saldo: res.order.Saldo
      });

      this.updateUrl();

      this.toastr.success('Se ha registrado el pedido exitosamente');
    }, (err: any) => {
      this.toastr.error(err.error || 'Ha ocurrido un error al guardar el pedido');
    });
  }

  attend(): void {
    let order = this.form.getRawValue();
    order.IdEstado = order.IdEstado || 1;

    if (order.IdEstado == 1) {
      this.isLoadingAttend = true;

      this.orderService.register(order, order.PedidoDetalle, true).subscribe(res => {
        this.isLoadingAttend = false;

        this.form.patchValue({
          IdPedido: res.order.IdPedido,
          IdEstado: res.order.IdEstado,
          Saldo: res.order.Saldo
        });

        this.updateUrl();

        if (res.inventory) {
          this.toastr.error('Hay algunos productos que no tienen stock suficiente para ser atendidos');

          const keys = Object.keys(res.inventory);

          this.products.controls.forEach((group: FormGroup) => {
            const id = group.get('IdProducto').value;

            if (keys.includes(id.toString())) {
              // const control = group.get('Stock');
              // control.setValue(res.inventory[id]);    
              group.get('Stock').setValue(res.inventory[id]);
              group.get('Cantidad').setErrors({ inventory: true });
            }
          });
        } else {
          order = this.form.getRawValue();
          if (order.IdFormaPago == 1) {
            this.bsModal.open(PaymentComponent, { order });
          } else {
            this.toastr.success('Se ha atendido el pedido al crédito', 'Listo');
          }
        }
      }, (err: any) => {
        this.isLoadingAttend = false;
        this.toastr.error(err.error || 'Ha ocurrido un error al atender el pedido');
      });
    } else {
      this.bsModal.open(PaymentComponent, { order });
    }
  }

  devolutions(): void {
    this.bsModal.open(DevolutionsComponent, { order: this.form.getRawValue() }).then(res => {
      if (res) {
        this.form.patchValue({
          Saldo: res.order.Saldo
        });

        res.devolutions.forEach(item => {
          const index = this.products.controls.findIndex(x => x.get('IdProducto').value == item.IdProducto);
          if (item.Cantidad == 0) {
            this.products.removeAt(index);
          } else {
            this.products.at(index).get('Cantidad').setValue(item.Cantidad);
          }
        });

        this.save();
      }
    });
  }

  invalidation(): void {
    const order = this.form.getRawValue();
    this.bsModal.open(InvalidationComponent, { order }).then(res => {
      if (res) {
        this.form.patchValue({
          IdEstado: res.order.IdEstado
        });

        this.toastr.success('Acaba de anular el pedido exitosamente', 'Anulado');
      }
    });
  }

  clear(): void {
    this.confirmService.open('Nuevo', '¿Desea abrir otra ventana o aquí mismo?',
      { confirmText: 'En esta ventana', cancelText: 'Otra ventana' }).then(success => {
        if (typeof success !== 'boolean') return;

        if (success) {
          this.form.reset({
            FlagAjustes: true,
            IdFormaPago: 1,
            IdMoneda: 'PEN',
            Moneda: 'S/',
          });

          this.products.clear();
          this.router.navigateByUrl('/ventas/registro');
        } else {
          window.open(`${window.location.origin}/ventas/registro`);
        }
      });
  }

  updateUrl(): void {
    this.location.go(`/ventas/registro/${this.form.get('IdPedido').value}`);
  }

  payments(): void {
    this.bsModal.open(MovementsComponent, { reference: 'PEDIDO', id_reference: this.form.get('IdPedido').value });
  }

  printer(): void {
    // const ticket = this.form.getRawValue();
    // let innerContents = this.render.create(TicketComponent, { ticket });
    // let popupWinindow = window.open('', '_blank', 'width=400,height=700,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no');
    // popupWinindow.document.open();
    // popupWinindow.document.write('<html><head></head><body onload="window.print(); window.close()">' + innerContents + '</html>');
    // popupWinindow.document.close();

    const order = this.form.getRawValue();
    order.PersonalRegistro = order.PersonalRegistro || this.session.user.username;
    this.orderService.ticket(order).subscribe(blob => {
      var url = (window.URL || window.webkitURL).createObjectURL(blob);
      // window.open(url);
      this.bsModal.open(TicketComponent, { blobUrl: url });
    });
  }

}
