import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesComponent } from './sales.component';
import { SalesRoutingModule } from './sales-routing.module';
import { SalesSearchComponent } from './views/search/search.component';
import { SalesRegisterComponent } from './views/register/register.component';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { ProductsSearchComponent } from './components/products-search/products-search.component';
import { PaymentComponent } from './components/payment/payment.component';
import { TicketComponent } from './components/ticket/ticket.component';
import { DevolutionsComponent } from './components/devolutions/devolutions.component';
import { InvalidationComponent } from './components/invalidation/invalidation.component';
import { CustomerComponentsModule } from '../customers/components/customer-components.module';

@NgModule({
  imports: [
    // CommonModule,
    SalesRoutingModule,
    AdminSharedModule,
    CustomerComponentsModule
  ],
  declarations: [
    SalesComponent,
    SalesSearchComponent,
    SalesRegisterComponent,
    ProductsSearchComponent,
    PaymentComponent,
    TicketComponent,
    DevolutionsComponent,
    InvalidationComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SalesModule { }
