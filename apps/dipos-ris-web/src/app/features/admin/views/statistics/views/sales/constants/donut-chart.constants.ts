import { ChartOptions } from "@admin-shared/types/chart-type";
import { colorPalette } from "./colors.constants";

export const DonutOptions: Partial<ChartOptions> = {
  chart: {
    type: 'donut',
    width: '100%',
    height: 400
  },
  dataLabels: {
    enabled: true,
    formatter: function (val: number) {
      return Math.round(val) + "%"
    }
  },
  yaxis: {
    axisBorder: {
      show: false
    },
    axisTicks: {
      show: false
    },
    labels: {
      formatter: (val) => {
        return val.toLocaleString();
      }
    }
  },
  plotOptions: {
    pie: {
      customScale: 0.9,
      donut: {
        size: '75%',
      },
      offsetY: 20,
    }
  },
  colors: colorPalette,
  title: {
    text: 'Ventas por categoría',
    style: {
      fontSize: '18px'
    }
  },
  series: [],
  labels: [],
  legend: {
    position: 'left',
    offsetY: 10
  },
  theme: {
    mode: 'light',
    palette: 'palette1',
    monochrome: {
      enabled: true,
      color: '#4723d9',
      shadeTo: 'light',
      shadeIntensity: 0.65
    },
  }
}