import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { StatisticsComponent } from "./statistics.component";
import { StatisticsSalesComponent } from "./views/sales/sales.component";

const routes: Routes = [
  {
    path: '',
    component: StatisticsComponent,
    children: [
      {
        path: '',
        component: StatisticsSalesComponent
      }      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StatisticsRoutingModule { }