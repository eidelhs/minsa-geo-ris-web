import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TacnaRisComponent } from './tacna-ris.component';

describe('TacnaRisComponent', () => {
  let component: TacnaRisComponent;
  let fixture: ComponentFixture<TacnaRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TacnaRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TacnaRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
