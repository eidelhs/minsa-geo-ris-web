import { ConfirmComponent } from "@admin-shared/components/confirm/confirm.component";
import { Injectable } from "@angular/core";
import { BsModalService } from "./bs-modal.service";

export interface IConfirmOptions {
  confirmText: string;
  cancelText: string;
}

@Injectable()
export class ConfirmService {

  constructor(
    private bsModal: BsModalService
  ) { }

  open(title: string, message: string, opt?: IConfirmOptions): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const options: IConfirmOptions = {
        confirmText: 'Aceptar',
        cancelText: 'Cancelar',
        ...opt
      };

      this.bsModal.open(ConfirmComponent, { title, message, ...options }).then(res => {
        if (resolve === null) reject();
        else resolve(res);
      });
    });
  }

}
