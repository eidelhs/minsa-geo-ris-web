import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {

  @Input() title: string = 'Confirmar';
  @Input() message: string;
  @Input() confirmText: string = 'Aceptar';
  @Input() cancelText: string = 'Cancelar';

  constructor(
    private bsModal: BsModalService
  ) { }

  ngOnInit() {
  }

  confirm(type: boolean): void {
    this.bsModal.close(type);
  }

}
