import { LoginComponent } from "@admin-shared/components/login/login.component";
import { BsModalService } from "@admin-shared/services/bs-modal.service";
import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { SessionService } from "@dipos-ris-web-commons";
import { Observable } from "rxjs";

@Injectable()
export class AuthChildsGuard implements CanActivateChild {

  constructor(
    private session: SessionService,
    private bsModal: BsModalService,
    private router: Router
  ) { }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    const isAuth = this.session.token;
    if (!isAuth) {
      // console.log('Tienes que logearte!! mostrar modal de inicio de sesion');
      this.bsModal.open(LoginComponent).then(res => {
        if (res) {
          this.router.navigateByUrl(state.url);
        }
      });
    }

    return !!isAuth;
  }


}
