import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WebsiteRoutingModule } from './website-routing.module';
import { InicioComponent } from './views/inicio/inicio.component';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { MapaRisComponent } from './views/mapa-ris/mapa-ris.component';
import { DashboardRisComponent } from './views/dashboard-ris/dashboard-ris.component';
import { ResolucionesConfirmacionRisComponent } from './views/resoluciones-confirmacion-ris/resoluciones-confirmacion-ris.component';
import { EquiposImpulsoresDeRisComponent } from './views/equipos-impulsores-de-ris/equipos-impulsores-de-ris.component';


@NgModule({
  declarations: [
    InicioComponent,
    MapaRisComponent,
    DashboardRisComponent,
    ResolucionesConfirmacionRisComponent,
    EquiposImpulsoresDeRisComponent
  ],
  imports: [
    //CommonModule,
    WebsiteRoutingModule,
    AdminSharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class WebsiteModule { }
