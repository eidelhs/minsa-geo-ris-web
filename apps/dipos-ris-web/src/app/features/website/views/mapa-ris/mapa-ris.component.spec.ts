import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MapaRisComponent } from './mapa-ris.component';

describe('MapaRisComponent', () => {
  let component: MapaRisComponent;
  let fixture: ComponentFixture<MapaRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MapaRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MapaRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
