import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ImpMatrizSeguimientoComponent } from './imp-matriz-seguimiento/imp-matriz-seguimiento.component';
import { VisorMetasFisicasComponent} from './visor-metas-fisicas/visor-metas-fisicas.component'
import { RegistrarMetasFisicasComponent} from './registrar-metas-fisicas/registrar-metas-fisicas.component'
import { SupervisarMetasFisicasComponent} from  './supervisar-metas-fisicas/supervisar-metas-fisicas.component'
import { PNIRISComponent } from './pniris.component';

const routes: Routes = [ {
  path: '',
  component: PNIRISComponent
},
{
  path :'importar-matriz-seguimiento',
  component: ImpMatrizSeguimientoComponent
},
{
  path :'app-visor-metas-fisicas',
  component: VisorMetasFisicasComponent
},
{
  path :'app-registrar-metas-fisicas',
  component: RegistrarMetasFisicasComponent
},
{
  path :'app-supervisar-metas-fisicas',
  component: SupervisarMetasFisicasComponent
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PNIRISRoutingModule { }
