import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisarMetasFisicasComponent } from './supervisar-metas-fisicas.component';

describe('SupervisarMetasFisicasComponent', () => {
  let component: SupervisarMetasFisicasComponent;
  let fixture: ComponentFixture<SupervisarMetasFisicasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupervisarMetasFisicasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisarMetasFisicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
