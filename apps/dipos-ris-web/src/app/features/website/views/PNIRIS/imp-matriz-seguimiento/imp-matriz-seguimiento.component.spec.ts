import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpMatrizSeguimientoComponent } from './imp-matriz-seguimiento.component';

describe('ImpMatrizSeguimientoComponent', () => {
  let component: ImpMatrizSeguimientoComponent;
  let fixture: ComponentFixture<ImpMatrizSeguimientoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImpMatrizSeguimientoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpMatrizSeguimientoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
