import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisorProgramacionPresupuestalComponent } from './visor-programacion-presupuestal.component';

describe('VisorProgramacionPresupuestalComponent', () => {
  let component: VisorProgramacionPresupuestalComponent;
  let fixture: ComponentFixture<VisorProgramacionPresupuestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisorProgramacionPresupuestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisorProgramacionPresupuestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
