import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProMetasFisicasComponent } from './pro-metas-fisicas.component';

describe('ProMetasFisicasComponent', () => {
  let component: ProMetasFisicasComponent;
  let fixture: ComponentFixture<ProMetasFisicasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProMetasFisicasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProMetasFisicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
