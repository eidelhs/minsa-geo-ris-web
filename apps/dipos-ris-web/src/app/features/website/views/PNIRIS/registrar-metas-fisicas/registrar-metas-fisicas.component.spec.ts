import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarMetasFisicasComponent } from './registrar-metas-fisicas.component';

describe('RegistrarMetasFisicasComponent', () => {
  let component: RegistrarMetasFisicasComponent;
  let fixture: ComponentFixture<RegistrarMetasFisicasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarMetasFisicasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarMetasFisicasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
