import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProPresupuestalComponent } from './pro-presupuestal.component';

describe('ProPresupuestalComponent', () => {
  let component: ProPresupuestalComponent;
  let fixture: ComponentFixture<ProPresupuestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProPresupuestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProPresupuestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
