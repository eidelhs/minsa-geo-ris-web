import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pniris',
  templateUrl: './pniris.component.html',
  styleUrls: ['./pniris.component.scss']
})
export class PNIRISComponent implements OnInit {

  constructor(private _router :Router) { }

  ngOnInit(): void {
  }


  goToImp(){
this._router.navigate(['PNIRIS/importar-matriz-seguimiento']);
  }



}
