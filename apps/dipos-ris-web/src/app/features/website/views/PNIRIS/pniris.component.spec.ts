import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PNIRISComponent } from './pniris.component';

describe('PNIRISComponent', () => {
  let component: PNIRISComponent;
  let fixture: ComponentFixture<PNIRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PNIRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PNIRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
