import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisarProgramacionPresupuestalComponent } from './supervisar-programacion-presupuestal.component';

describe('SupervisarProgramacionPresupuestalComponent', () => {
  let component: SupervisarProgramacionPresupuestalComponent;
  let fixture: ComponentFixture<SupervisarProgramacionPresupuestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupervisarProgramacionPresupuestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisarProgramacionPresupuestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
