import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarProgramacionPresupuestalComponent } from './registrar-programacion-presupuestal.component';

describe('RegistrarProgramacionPresupuestalComponent', () => {
  let component: RegistrarProgramacionPresupuestalComponent;
  let fixture: ComponentFixture<RegistrarProgramacionPresupuestalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrarProgramacionPresupuestalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarProgramacionPresupuestalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
