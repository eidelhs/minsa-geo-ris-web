import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquiposImpulsoresDeRisComponent } from './equipos-impulsores-de-ris.component';

describe('EquiposImpulsoresDeRisComponent', () => {
  let component: EquiposImpulsoresDeRisComponent;
  let fixture: ComponentFixture<EquiposImpulsoresDeRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquiposImpulsoresDeRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquiposImpulsoresDeRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
