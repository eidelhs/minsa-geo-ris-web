import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolucionesConfirmacionRisComponent } from './resoluciones-confirmacion-ris.component';

describe('ResolucionesConfirmacionRisComponent', () => {
  let component: ResolucionesConfirmacionRisComponent;
  let fixture: ComponentFixture<ResolucionesConfirmacionRisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResolucionesConfirmacionRisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolucionesConfirmacionRisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
