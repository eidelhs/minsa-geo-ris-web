import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { EditarComponent } from './editar/editar.component';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {

  quick_label: String = 'Unidad Orgánica:';
  formSearch: FormGroup;
  form: FormGroup;
  ris = new FormControl();
  esris = [];
  paginationController: any = {};
  users: any[] = [
    {direccionGeneral : "DIRECCION GENERAL DE ASEGURAMIENTO E INTERCAMBIO PRESTACIONAL", UnOrganica : "DIRECCIÓN DE INTERCAMBIO PRESTACIONAL, ORGANIZACION Y SERVICIOS DE SALUD" ,NomApe : "SARAVIA CAHUANA SILVIA" ,dni : "08863860" ,cargo : "DIRECTORA EJECUTIVA" , condLaboral : "CAS"  },
    {direccionGeneral : "DIRECCION GENERAL DE ASEGURAMIENTO E INTERCAMBIO PRESTACIONAL", UnOrganica : "DIRECCIÓN DE INTERCAMBIO PRESTACIONAL, ORGANIZACION Y SERVICIOS DE SALUD" ,NomApe : "SARAVIA CAHUANA SILVIA" ,dni : "08863860" ,cargo : "DIRECTORA EJECUTIVA" , condLaboral : "CAS"  },
    {direccionGeneral : "DIRECCION GENERAL DE ASEGURAMIENTO E INTERCAMBIO PRESTACIONAL", UnOrganica : "DIRECCIÓN DE INTERCAMBIO PRESTACIONAL, ORGANIZACION Y SERVICIOS DE SALUD" ,NomApe : "SARAVIA CAHUANA SILVIA" ,dni : "08863860" ,cargo : "DIRECTORA EJECUTIVA" , condLaboral : "CAS"  },
    {direccionGeneral : "DIRECCION GENERAL DE ASEGURAMIENTO E INTERCAMBIO PRESTACIONAL", UnOrganica : "DIRECCIÓN DE INTERCAMBIO PRESTACIONAL, ORGANIZACION Y SERVICIOS DE SALUD" ,NomApe : "SARAVIA CAHUANA SILVIA" ,dni : "08863860" ,cargo : "DIRECTORA EJECUTIVA" , condLaboral : "CAS"  },
    {direccionGeneral : "DIRECCION GENERAL DE ASEGURAMIENTO E INTERCAMBIO PRESTACIONAL", UnOrganica : "DIRECCIÓN DE INTERCAMBIO PRESTACIONAL, ORGANIZACION Y SERVICIOS DE SALUD" ,NomApe : "SARAVIA CAHUANA SILVIA" ,dni : "08863860" ,cargo : "DIRECTORA EJECUTIVA" , condLaboral : "CAS"  }
  ];
 
  isLoading: boolean;
  changeFilter: boolean = true;
  url = "";
  urlSafe: SafeResourceUrl;
  constructor( 
    private fb: FormBuilder,
    private bsModal: BsModalService,
   // private productService: ProductService,
    public sanitizer: DomSanitizer) { 
    this.builder();
  }

  ngOnInit() {
    this.formSearch.get('quick').setValue(1);
    // this.productService.trademarks().subscribe(res => {
    //   this.esris = res;
    // });
    // this.search();
  }

  builder(): void {
    this.formSearch = this.fb.group({
      option: 1,
      quick: null,
      ris: null,
      genero: null,
      dni: null,
      nombres: null,
      paterno: null,
      materno: null,
      edad: null
    });


  }

  onChange($event){
    // const valor = $event.target.value;
    // if(valor == '1') {
    //   this.quick_label = 'Nombre de RIS:';
    //   this.changeFilter = true;
    // }else if(valor == '2') {
    //   this.quick_label = 'Nº de RIS:';
    //   this.changeFilter = false;
    // }
    // this.productService.trademarks().subscribe(res => {
    //   this.esris = res;
    // });
  }

  search() {
  
    // let typeFilter = this.changeFilter?1:2;
    // this.productService.resultados(this.formSearch.value,typeFilter).subscribe(res => {
    //   this.orders = res;
   
    // });
  }

  clean() {
    this.users = [];
    this.formSearch.get('quick').setValue(1);
    this.formSearch.get('ris').setValue(null);
    this.formSearch.get('dni').setValue(null);
    this.formSearch.get('nombres').setValue(null);
    this.formSearch.get('paterno').setValue(null);
    this.formSearch.get('materno').setValue(null);
    this.formSearch.get('genero').setValue(null);
    this.formSearch.get('edad').setValue(null);
    this.quick_label = 'Nombre de RIS:';
    this.changeFilter = true;
    this.url = "";
    this.urlSafe= this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }

  paginationChange(e: any): void {
    this.paginationController = e;
  }

  options(option , item = null) {

    switch(option){
      case 1 :   this.bsModal.open(EditarComponent, { item }).then(res => { }); break;
      case 2 : console.log("delete"); break;
    }

  
  }

}
