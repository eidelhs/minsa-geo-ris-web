import { BsModalService } from '@admin-shared/services/bs-modal.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss'],
})
export class EditarComponent implements OnInit {
  @Input() user: any;
  listDirGeneral: any = [];
  listUniOrganica: any = [];
  listTipDoc :any = [{ id: 1, name: 'DNI' },{ id: 2, name: 'C. E.' }, { id: 3, name: 'Pasaporte' }];
  listGenero : any = [{id: 'M', name :'Masculino'}, {id:'F', name: 'Femenino'}];
  listCargo : any = [];
  listCondicionLab : any = [];
  form: FormGroup;
  constructor(private fb: FormBuilder, private bsModal: BsModalService) {
    this.build();
  }

  ngOnInit(): void {

    this.listDireccionGeneralinit();
    this.listUnidadOrganicainit();
    this.listCargoinit();
    this.listCondicionLabinit();

  }

  build(): void {
    this.form = this.fb.group({
      dirGeneral: [1, Validators.required],
      uOrganica: [1, Validators.required],
      nombres: [null, Validators.required],
      aPaterno: [null, Validators.required],
      aMaterno: [null, Validators.required],
      tipoDocumento: [1, Validators.required],
      documento: [null, Validators.required],
      genero: ['M', Validators.required],
      fNacimiento: [null, Validators.required],
      cargo: [1, Validators.required],
      cLaboral: [1, Validators.required],
      FlagActivo: true,
    });
  }

  save() {}

  listDireccionGeneralinit() {
    this.listDirGeneral = [{ id: 1, name: 'prueba 1' },
    { id: 2, name: 'prueba 2' },
    { id: 3, name: 'prueba 3' },
    { id: 4, name: 'prueba 4' }];
  }

  listUnidadOrganicainit() {
    this.listUniOrganica = [{ id: 1, name: 'prueba 1' },
    { id: 2, name: 'prueba 2' },
    { id: 3, name: 'prueba 3' },
    { id: 4, name: 'prueba 4' }];
  }

  listCargoinit() {
    this.listCargo = [{ id: 1, name: 'prueba 1' },
    { id: 2, name: 'prueba 2' },
    { id: 3, name: 'prueba 3' },
    { id: 4, name: 'prueba 4' }];
  }
  

  listCondicionLabinit() {
    this.listCondicionLab = [{ id: 1, name: 'prueba 1' },
    { id: 2, name: 'prueba 2' },
    { id: 3, name: 'prueba 3' },
    { id: 4, name: 'prueba 4' }];
  }


}
