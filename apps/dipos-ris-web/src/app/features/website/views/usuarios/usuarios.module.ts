import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsuariosRoutingModule } from './usuarios-routing.module';
import { UsuariosComponent } from './usuarios.component';
import { NgApexchartsModule } from 'ng-apexcharts';
import { AuthChildsGuard } from '../../../admin/guards/auth-childs.guard';
import { AdminSharedModule } from '@admin-shared/shared.module';
import { EditarComponent } from './editar/editar.component';


@NgModule({
  declarations: [
    UsuariosComponent,
    EditarComponent
    
  ],
  imports: [
    //CommonModule,
    UsuariosRoutingModule,
    AdminSharedModule,
    NgApexchartsModule

  ],  
  providers: [
    AuthChildsGuard
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class UsuariosModule { }
