import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Etapa0RoutingModule } from './etapa-0-routing.module';
import { EtapaCeroComponent } from './etapa-cero.component';


@NgModule({
  declarations: [
    EtapaCeroComponent
  ],
  imports: [
    CommonModule,
    Etapa0RoutingModule
  ]
})
export class Etapa0Module { }
