import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FasesComponent } from './fases.component';

const routes: Routes =  [
  {
		path: '',
		component: FasesComponent,
		//canActivateChild: [AuthChildsGuard],
		children: [
      {
				path: 'fase-1',
				loadChildren: () => import('./fase-1/fase-1.module').then(m => m.Fase1Module)
			},
      {
				path: 'fase-2',
				loadChildren: () => import('./fase-2/fase-2.module').then(m => m.Fase2Module)
			},
      {
				path: 'fase-3',
				loadChildren: () => import('./fase-3/fase-3.module').then(m => m.Fase3Module)
			},
      {
				path: 'fase-4',
				loadChildren: () => import('./fase-4/fase-4.module').then(m => m.Fase4Module)
			},
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FasesRoutingModule { }
