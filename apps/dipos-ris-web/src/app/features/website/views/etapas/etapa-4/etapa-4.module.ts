import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Etapa4RoutingModule } from './etapa-4-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Etapa4RoutingModule
  ]
})
export class Etapa4Module { }
