import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EtapasRoutingModule } from './etapas-routing.module';
import { EtapasComponent } from './etapas.component';


@NgModule({
  declarations: [
    EtapasComponent
  ],
  imports: [
    CommonModule,
    EtapasRoutingModule
  ]
  ,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EtapasModule { }
