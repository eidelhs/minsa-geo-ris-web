import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanTrabajoCOINRISComponent } from './plan-trabajo-coinris.component';

describe('PlanTrabajoCOINRISComponent', () => {
  let component: PlanTrabajoCOINRISComponent;
  let fixture: ComponentFixture<PlanTrabajoCOINRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanTrabajoCOINRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanTrabajoCOINRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
