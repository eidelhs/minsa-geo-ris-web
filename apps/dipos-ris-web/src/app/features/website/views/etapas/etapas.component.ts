import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-etapas',
  templateUrl: './etapas.component.html',
  styleUrls: ['./etapas.component.scss']
})
export class EtapasComponent implements OnInit {

  show: boolean = true;
  offline: boolean;

  constructor() { }

  ngOnInit() {
    const sidebar = localStorage.getItem('sidebar');
    this.show = !sidebar || sidebar === 'open';

    this.verifySignal();
  }

  verifySignal(): void {
    fromEvent(window, 'online')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = false);

    fromEvent(window, 'offline')
      .pipe(
        debounceTime(250)
      ).subscribe(() => this.offline = true);
  }

}
