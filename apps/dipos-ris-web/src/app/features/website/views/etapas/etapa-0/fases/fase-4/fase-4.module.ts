import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Fase4RoutingModule } from './fase-4-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Fase4RoutingModule
  ]
})
export class Fase4Module { }
