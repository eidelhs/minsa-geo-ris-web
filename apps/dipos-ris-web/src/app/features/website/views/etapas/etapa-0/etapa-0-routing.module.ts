import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EtapaCeroComponent } from './etapa-cero.component';

const routes: Routes =[
  {
		path: '',
		component: EtapaCeroComponent,
		//canActivateChild: [AuthChildsGuard],
		children: [
			{
				path: 'fases',
				loadChildren: () => import('./fases/fases.module').then(m => m.FasesModule)
			}
		]
	}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Etapa0RoutingModule { }
