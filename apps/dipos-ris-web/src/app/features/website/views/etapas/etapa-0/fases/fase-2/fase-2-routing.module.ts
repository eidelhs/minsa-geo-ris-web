import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboComImpNacRISComponent } from './abo-com-imp-nac-ris/abo-com-imp-nac-ris.component';
import { DocNormEspRegModRISComponent } from './doc-norm-esp-reg-mod-ris/doc-norm-esp-reg-mod-ris.component';
import { DocNormRelDimFinModRISComponent } from './doc-norm-rel-dim-fin-mod-ris/doc-norm-rel-dim-fin-mod-ris.component';
import { DocNormRelDimGestModRISComponent } from './doc-norm-rel-dim-gest-mod-ris/doc-norm-rel-dim-gest-mod-ris.component';
import { DocNormRelDimGobModRISComponent } from './doc-norm-rel-dim-gob-mod-ris/doc-norm-rel-dim-gob-mod-ris.component';
import { DocNormRelDimPresModRISComponent } from './doc-norm-rel-dim-pres-mod-ris/doc-norm-rel-dim-pres-mod-ris.component';
import { RMCOINRISComponent } from './rm-coinris/rm-coinris.component';
import { PlanTrabajoCOINRISComponent } from './plan-trabajo-coinris/plan-trabajo-coinris.component';
import { ActasCOINRISComponent } from './actas-coinris/actas-coinris.component';
import { InformesCOINRISComponent } from './informes-coinris/informes-coinris.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', component: DocNormEspRegModRISComponent },
  {
    path: 'Documentos-Normativos-Especificos-Regularizacion-Modelo-RIS',
    component: DocNormEspRegModRISComponent
  },
  {
    path: 'Documentos-Normativos-Relacionados-Dimension-Prestacion-Modelo-RIS',
    component: DocNormRelDimPresModRISComponent
  },
  {
    path: 'Documentos-Normativos-Relacionados-Dimension-Gestion-Modelo-RIS',
    component: DocNormRelDimGestModRISComponent
  },
  {
    path: 'Documentos-Normativos-Relacionados-Dimension-Financiamiento-Modelo-RIS',
    component: DocNormRelDimFinModRISComponent
  },
  {
    path: 'Documentos-Normativos-Relacionados-Dimension-Gobernanza-Modelo-RIS',
    component: DocNormRelDimGobModRISComponent
  },
  {
    path: 'Abogacia-Comite-Impulsor-Nacional-RIS',
    component: AboComImpNacRISComponent
  },
  {
    path: 'RM-COINRIS',
    component: RMCOINRISComponent
  },
  {
    path: 'Plan-Trabajo-COINRIS',
    component: PlanTrabajoCOINRISComponent
  },
  {
    path: 'Actas-COINRIS',
    component: ActasCOINRISComponent
  },
  {
    path: 'Informes-COINRIS',
    component: InformesCOINRISComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Fase2RoutingModule { }
