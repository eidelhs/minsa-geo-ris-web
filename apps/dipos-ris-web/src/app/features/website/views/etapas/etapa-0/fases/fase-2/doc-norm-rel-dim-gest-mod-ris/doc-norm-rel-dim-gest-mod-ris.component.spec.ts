import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocNormRelDimGestModRISComponent } from './doc-norm-rel-dim-gest-mod-ris.component';

describe('DocNormRelDimGestModRISComponent', () => {
  let component: DocNormRelDimGestModRISComponent;
  let fixture: ComponentFixture<DocNormRelDimGestModRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocNormRelDimGestModRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocNormRelDimGestModRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
