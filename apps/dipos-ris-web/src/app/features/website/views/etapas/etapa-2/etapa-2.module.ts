import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Etapa2RoutingModule } from './etapa-2-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    Etapa2RoutingModule
  ]
})
export class Etapa2Module { }
