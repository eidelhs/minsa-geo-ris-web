import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DocNormRelDimFinModRISComponent } from './doc-norm-rel-dim-fin-mod-ris.component';

describe('DocNormRelDimFinModRISComponent', () => {
  let component: DocNormRelDimFinModRISComponent;
  let fixture: ComponentFixture<DocNormRelDimFinModRISComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DocNormRelDimFinModRISComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DocNormRelDimFinModRISComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
