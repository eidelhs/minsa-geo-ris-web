import { NgModule } from '@angular/core';
import { ClickOutDirective } from './clickOut.directive';
import { InfiniteScrollDirective } from './infinite-scroll.directive';
import { LtDragDirective } from './lt-drag.directive';

const DIRECTIVES = [
  ClickOutDirective,
  InfiniteScrollDirective,
  LtDragDirective
];

@NgModule({
  declarations: [...DIRECTIVES],
  exports: [...DIRECTIVES]
})
export class MarketDirectivesModule { }
