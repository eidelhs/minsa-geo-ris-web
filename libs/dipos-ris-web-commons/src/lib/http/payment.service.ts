import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class PaymentService {

  private apiBase = environment.apiURL

  constructor(private http: HttpClient) { }

  search(bodyRequest: any): Observable<any[]> {
    return this.http.post<any[]>(`${this.apiBase}/api/payments/search`, bodyRequest);
  }

  get(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/api/payments/${id}`)
      .pipe(
        map(
          res => {
            if (res.FechaRegistro) res.FechaRegistro = new Date(res.FechaRegistro);
            if (res.FechaModificacion) res.FechaModificacion = new Date(res.FechaModificacion);
            if (res.FechaAnulacion) res.FechaAnulacion = new Date(res.FechaAnulacion);
            if (res.FechaVencimiento) res.FechaVencimiento = new Date(res.FechaVencimiento);
            if (res.FechaPago) res.FechaPago = new Date(res.FechaPago);

            return res;
          }
        )
      );
  }

  register(payment: any): Observable<number> {
    return this.http.post<number>(`${this.apiBase}/api/payments/register`, payment);
  }

  cancel(payment_id: number): Observable<boolean> {
    return this.http.post<boolean>(`${this.apiBase}/api/payments/cancel`, payment_id);
  }

  pay(payment: any): Observable<boolean> {
    return this.http.post<boolean>(`${this.apiBase}/api/payments/pay`, payment);
  }

}