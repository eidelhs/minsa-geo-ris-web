import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";

@Injectable()
export class MoneyboxService {

  private apiBase = `${environment.apiURL}/api/money-box`

  constructor(private http: HttpClient) { }

  getAll(state?: number): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}?state=${state}`);
  }

  getCurrencies(): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/currencies`);
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/${id}`);
  }

  getConcepts(type?: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.apiBase}/concepts?type=${type}`);
  }

  getMovements(bodyRequest: { id_caja?: number, date_op?: string, reference?: string, id_reference?: string }): Observable<any> {
    const params = new HttpParams()
      .set('id_caja', bodyRequest.id_caja || null)
      .set('date_op', bodyRequest.date_op || '')
      .set('reference', bodyRequest.reference || '')
      .set('id_reference', bodyRequest.id_reference || '');

    return this.http.get<any>(`${this.apiBase}/movements?${params.toString()}`);
  }

  movementInsert(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiBase}/movements/insert`, data);
  }

}