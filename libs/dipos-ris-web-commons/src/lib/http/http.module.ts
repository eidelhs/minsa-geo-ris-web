import { NgModule } from "@angular/core";
import { HttpClientModule } from '@angular/common/http'
import { ProductService } from "./product.service";
import { WarehouseService } from "./warehouse.service";
import { AuthService } from "./auth.service";
import { PersonalService } from "./personal.service";
import { UserService } from "./user.service";
import { MoneyboxService } from "./moneybox.service";
import { OrderService } from "./order.service";
import { OwnerService } from "./owner.service";
import { StatisticsService } from "./statistics.service";
import { ReportsService } from "./reports.service";
import { PaymentService } from "./payment.service";

@NgModule({
  imports: [
    HttpClientModule
  ],
  providers: [
    AuthService,
    ProductService,
    WarehouseService,    
    PersonalService,
    UserService,
    MoneyboxService,
    PaymentService,
    OrderService,
    OwnerService,
    StatisticsService,
    ReportsService
  ]
})
export class MarketHttpModule { }
