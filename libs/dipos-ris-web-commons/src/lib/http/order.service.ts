import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class OrderService {

  private apiBase = environment.apiURL

  constructor(private http: HttpClient) { }

  search(bodyRequest: any): Observable<any[]> {
    return this.http.post<any[]>(`${this.apiBase}/api/order/search`, bodyRequest);
  }

  getById(id: number): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/api/order/${id}`)
      .pipe(
        map(res => {
          if (res.FechaPago) res.FechaPago = new Date(res.FechaPago);
          if (res.FechaRegistro) res.FechaRegistro = new Date(res.FechaRegistro);

          res.PedidoDetalle.forEach(p => {
            p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
          });

          return res;
        })
      );
  }

  register(order: any, detail: any, flgAttend?: boolean): Observable<any> {
    return this.http.post<any>(`${this.apiBase}/api/order/register?flgAttend=${flgAttend ?? false}`, { order, detail });
  }

  devolution(order: any, movements: any[], id_box?: number, mount?: number): Observable<any> {
    const request = {
      Pedido: order,
      AlmacenMovimientos: movements,
      IdCaja: id_box,
      MontoDevolucion: mount
    };

    return this.http.post<any>(`${this.apiBase}/api/order/devolution`, request);
  }

  invalidation(order: any, movements: any[], id_box?: number, mount?: number): Observable<any> {
    const request = {
      Pedido: order,
      AlmacenMovimientos: movements,
      IdCaja: id_box,
      MontoDevolucion: mount
    };

    return this.http.post<any>(`${this.apiBase}/api/order/invalidation`, request);
  }

  /* ticket(html: string): Observable<any> {
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded')
      .set('Accept', 'application/pdf');

    return this.http.post<any>(`${this.apiBase}/api/order/ticket`, `=${html}`, { headers, responseType: 'blob' as any });
  } */

  ticket(order: any): Observable<any> {
    let headers = new HttpHeaders()
      .set('Accept', 'application/pdf');

    return this.http.post<any>(`${this.apiBase}/api/order/ticket`, order, { headers, responseType: 'blob' as any });
  }

}
