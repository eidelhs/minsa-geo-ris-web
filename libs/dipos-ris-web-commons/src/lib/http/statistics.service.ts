import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "apps/dipos-ris-web/src/environments/environment";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class StatisticsService {

  private apiBase = `${environment.apiURL}`;

  constructor(private http: HttpClient) { }

  sales(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    const headers = { context: '.box-sales' };
    return this.http.get<any[]>(`${this.apiBase}/api/statistics/sales?${params.toString()}`, { headers });
  }

  salesByHorary(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    const headers = { context: '.box-sales-by-time' };
    return this.http.get<any[]>(`${this.apiBase}/api/statistics/sales-by-time?${params.toString()}`, { headers });
  }

  salesByCategory(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    const headers = { context: '.box-sales-by-category' };
    return this.http.get<any[]>(`${this.apiBase}/api/statistics/sales-by-category?${params.toString()}`, { headers });
  }

  buys(start: string, end: string): Observable<any[]> {
    const params = new HttpParams()
      .set('start', start)
      .set('end', end);

    const headers = { context: '.box-buys' };
    return this.http.get<any[]>(`${this.apiBase}/api/statistics/buys?${params.toString()}`, { headers });
  }

  resume(): Observable<any> {
    return this.http.get<any>(`${this.apiBase}/api/statistics/dashboard`)
      .pipe(
        map(res => {
          res.Products = res.Products.map((p: any) => {
            p.image = `${this.apiBase}/content/products/${p.IdProducto}.png?v=${new Date().getTime()}`;
            return p;
          });

          return res;
        }
        )
      );
  }

}