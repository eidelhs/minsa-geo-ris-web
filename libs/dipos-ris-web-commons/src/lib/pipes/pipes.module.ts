import { NgModule } from '@angular/core';
import { YesnoPipe } from './yesno.pipe';
import { PadLeftPipe } from './padleft.pipe';
import { SafeHtmlPipe } from './safeHtml.pipe';
import { SafeUrlPipe } from './safeUrl.pipe';
import { AsUrlPipe } from './asUrl.pipe';
import { FileSizePipe } from './fileSize.pipe';

const PIPES = [
	YesnoPipe,
	PadLeftPipe,
	SafeHtmlPipe,
	SafeUrlPipe,
	AsUrlPipe,
	FileSizePipe
];

@NgModule({
	exports: [
		...PIPES
	],
	declarations: [
		...PIPES
	]
})
export class MarketPipesModule { }
