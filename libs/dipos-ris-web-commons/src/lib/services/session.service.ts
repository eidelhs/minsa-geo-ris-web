import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserModel, TokenDecoded, TokenModel } from '../models/user.model';
import { CryptoService } from './crypto.service';
// import { JwtHelperService } from "@auth0/angular-jwt";

// Una sola instancia del servicio en toda la aplicacion: providedIn: 'root'
@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private _key = 'gBr77yPwYG';
  private _storage: TokenDecoded;
  // private helper = new JwtHelperService();

  constructor(private crypto: CryptoService) { }

  get user(): UserModel {
    return this.token ? new UserModel(this.token) : null;
  }

  get token(): any {
    try {
      const storedToken = this.tokenStored;
      const tokenModel = new TokenModel(storedToken);
      return storedToken;
      //return tokenModel.isExpired ? null : storedToken;
    } catch (e) {
      return null;
    }
  }

  // Para realizar algunas actualizaciones al token
  get tokenStored(): TokenDecoded {
    try {
      this._storage = this._storage ||
        JSON.parse(this.crypto.get(localStorage.getItem(this._key))) as TokenDecoded;
      return this._storage;
      // return JSON.parse(this.crypto.get(localStorage.getItem(this._key))) as TokenDecoded;
    } catch (e) {
      return null;
    }
  }

  create(token: string | TokenDecoded) {
    if (typeof token !== 'string') {
      token = JSON.stringify(token);
    }

    this._storage = null;
    localStorage.setItem(this._key, this.crypto.set(token));
  }

  destroy() {
    this._storage = null;
    localStorage.removeItem(this._key);
  }

}
