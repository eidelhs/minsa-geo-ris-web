import { NgModule } from "@angular/core";
import { CryptoService } from "./crypto.service";
import { DomService } from "./dom.service";
import { RenderComponentService } from "./render.service";
// import { SessionService } from "./session.service";

@NgModule({
  providers: [
    DomService,
    CryptoService,
    RenderComponentService,
    // SessionService
  ]
})
export class MarketServicesModule { }
